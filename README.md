### Available classes in this library

- \Library\HttpClientTrait<br>
  Trait for adding a httpClient property to a class.<br>
  [code examples](docs/HttpClientTrait.md)

- \Library\LoggerTrait<br>
  Trait for adding a logger property to a class.<br>
  [code examples](docs/LoggerTrait.md)

- \Library\Webquery<br>
  Class for sending requests to a WebQuery service and processing the responses.<br>
  [code examples](docs/WebQuery.md)

- \Library\Test\MockHttpClient<br>
  A http client class to mock http responses in test code.

A subclass of \GuzzleHttp\Client with a mock handler
and request history middleware.
Useful for writing tests.

### Using the library in other projects
To use this repository as a dependency in composer.json
add the following lines:
```javascript
    "repositories": [
        {
            "type": "gitlab",
            "url": "https://git.wur.nl/library/php-modules.git"
        }
    ],
```
This instructs composer to check the library/php-modules repository.
For composer to be able the correctly access the repository it must
be told that it is een alternative gitlab server (domain):
```javascript
    "config": {
        "gitlab-domains": ["git.wur.nl"]
    },
```
And composer needs a personal access token to authenticate at the git.wur.nl server.
Create a personal access token on the git.wur.nl server (in your profile)
and at it to the file '$HOME/.composer/auth.json':
```javascript
{
    "bitbucket-oauth": {},
    "github-oauth": {},
    "gitlab-oauth": {},
    "gitlab-token": { "git.wur.nl": "my-secret-access-token" }
}
```
Now you can at the dependency:
```javascript
    "require": {
        "library/php-modules": "^1.0"
    },
```
and run 'composer install' or 'composer update'.

### Running Tests
During development you can run tests using the phpunit library. You can use composer to start the test:
```composer test```
Make sure you test the code before checking it in.

### Vagrant
To make sure you have a production-like environment for testing and developing, a Vagrantfile is available in the project root directory.
If vagrant [has been installed](https://www.vagrantup.com/docs/installation/) locally:
```
cd {projectdir}
vagrant up
```
Use ```vagrant status``` for more info about the image.
To ssh into the image use: ```vagrant ssh```
The projectdirectory is mounted in the /vagrant directory in the image. Don't forget to run ```composer install``` after the first build inside the vagrant directory.
