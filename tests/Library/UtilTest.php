<?php

// phpcs:disable Generic.Files.LineLength

namespace Tests\Library;

class UtilTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @dataProvider dataValueOfXpath
     */
    public function testValueOfXpath($xml, $xpath, $default, $expect, $comment)
    {
        if (empty($default)) {
            $this->assertEquals($expect, \Library\Util::valueOfXpath($xml, $xpath), "without default: $comment");
        } else {
            $this->assertEquals($expect, \Library\Util::valueOfXpath($xml, $xpath, $default), "with default: $comment");
        }
    }

    public function dataValueOfXpath()
    {
        $xml = simplexml_load_string("<r><a><b>b1</b><c>c1</c><c>c2</c></a></r>");
        return [
            [$xml, "a/b", null, "b1", "value of b"],
            [$xml, "a/b", "b0", "b1", "value of b"],
            [$xml, "a/c", null, "c1", "1st value of c"],
            [$xml, "a/c[2]", null, "c2", "2nd value of c"],
            [$xml, "a/d", null, "", "no d"],
            [$xml, "a/d", "d0", "d0", "no d"],
        ];
    }
}
