<?php

namespace Tests\Library;

use GuzzleHttp\Psr7\Response;

class HttpClient
{
    use \Library\HttpClientTrait;

    // make public for testing
    public function publicGetMergedHttpOptions($options)
    {
        return $this->getMergedHttpOptions($options);
    }
}
