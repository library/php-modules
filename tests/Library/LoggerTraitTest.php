<?php

// phpcs:disable Generic.Files.LineLength

namespace Tests\Library;

class LoggerTraitTest extends \PHPUnit\Framework\TestCase
{
    public function testDefaultInitLogger()
    {
        $testlog = new class {
            use \Library\LoggerTrait;
        };
        $this->assertNull($testlog->getLogger(), "no logger without initLogger()");

        $testlog->initLogger([]);
        $logger = $testlog->getLogger();
        $this->assertInstanceOf("\Monolog\Logger", $logger, 'getLogger() returns instance of \Monolog\Logger');
        $this->assertEquals("default", $logger->getName(), "default name");
        $handlers = $logger->getHandlers();
        $this->assertEquals(1, count($handlers), "default logger has 1 handler");
        $handler = $handlers[0];
        $this->assertInstanceOf("\Monolog\Handler\StreamHandler", $handler, 'default log handler is a StreamHandler');
        $this->assertEquals("php://stderr", $handler->getUrl(), "default log handler path (url)");
        $this->assertEquals(\Monolog\Logger::toMonologLevel("info"), $handler->getLevel(), "default log handler level is 'info'");
        $formatter = $handler->getFormatter();
        $this->assertInstanceOf("\Monolog\Formatter\LineFormatter", $formatter, 'default log handler formatter is a LineFormatter');
    }


    public function testInitLoggerSingleHandler()
    {
        $settings = [
            "name" => "test-logger",
            "path" => "log/test.log",
            "level" => "debug",
            "format" => "[%datetime%]%level_name% %extra% %context%: %message%", // not testable
        ];

        $testlog = new class {
            use \Library\LoggerTrait;
        };
        $testlog->initLogger($settings);
        $logger = $testlog->getLogger();
        $this->assertEquals($settings["name"], $logger->getName(), "name");
        $handlers = $logger->getHandlers();
        $this->assertEquals(1, count($handlers), "logger has 1 handler");
        $handler = $handlers[0];
        $this->assertEquals(getcwd() . "/./" . $settings["path"], $handler->getUrl(), "log handler path (with default basedir)");
        $this->assertEquals(\Monolog\Logger::toMonologLevel($settings["level"]), $handler->getLevel(), "log handler level");

        $basedir = "/var/log/local";
        $testlog = new class {
            use \Library\LoggerTrait;
        };
        $testlog->initLogger($settings, $basedir);
        $logger = $testlog->getLogger();
        $this->assertEquals($settings["name"], $logger->getName(), "name");
        $handlers = $logger->getHandlers();
        $this->assertEquals(1, count($handlers), "logger has 1 handler");
        $handler = $handlers[0];
        $this->assertEquals("$basedir/" . $settings["path"], $handler->getUrl(), "log handler path (with passed basedir)");
        $this->assertEquals(\Monolog\Logger::toMonologLevel($settings["level"]), $handler->getLevel(), "log handler level");
    }


    public function testInitLoggerTwoHandlers()
    {
        $basedir = "/var/log/local";
        $settings = [
            "name" => "test-logger",
            "handler" => [
                [
                    "path" => "log/test.log",
                    "expectpath" => "$basedir/log/test.log", // expected path for testing
                    "level" => "debug",
                    "format" => "[%datetime%]%level_name% %extra% %context%: %message%", // not testable
                ],
                [
                    "path" => "php://stdout",
                    "expectpath" => "php://stdout",
                    "level" => "warning",
                    "format" => "[%datetime%]%level_name% %extra% %context%: %message%", // not testable
                ],
            ],
        ];

        $testlog = new class {
            use \Library\LoggerTrait;
        };
        $testlog->initLogger($settings, $basedir);
        $logger = $testlog->getLogger();
        $this->assertEquals($settings["name"], $logger->getName(), "name");
        $expects = $settings["handler"];
        $handlers = $logger->getHandlers();
        $this->assertEquals(count($expects), count($handlers), "number of handlers in settings");
        for ($i = 0; $i < count($expects); $i++) {
            $expect = $expects[$i];
            $handler = array_pop($handlers);  // handlers are in reverse order
            $this->assertEquals($expect["expectpath"], $handler->getUrl(), "$i: log handler path");
            $this->assertEquals(\Monolog\Logger::toMonologLevel($expect["level"]), $handler->getLevel(), "$i: log handler level");
        }
    }
}
