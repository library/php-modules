<?php

// phpcs:disable Generic.Files.LineLength

namespace Tests\Library;

use GuzzleHttp\Psr7\Response;
use Library\Webquery;

class WebqueryTest extends \PHPUnit\Framework\TestCase
{
    public function testAttributes()
    {
        // test webquery specific attributes
        // for which also getters and setters exist

        $wq = new Webquery();
        $expect = [
            "host" => "backend.library.wur.nl",
            "suffix" => "xml",
        ];
        $this->assertEquals($expect, $wq->getAttributes(), 'default attributes');

        $attribs = [
            "host" => "devel.library.wur.nl",
            "service" => "any",
        ];
        $wq->setAttributes($attribs);
        $this->assertEquals($attribs, $wq->getAttributes(), 'attributes after setAttributes()');

        $wq = new Webquery();
        $attribs = [
            "host" => "devel.library.wur.nl",
            "service" => "any",
        ];
        $expect = [
            "host" => "devel.library.wur.nl",
            "service" => "any",
            "suffix" => "xml",
        ];
        $wq->addAttributes($attribs);
        $this->assertEquals($expect, $wq->getAttributes(), 'attributes after addAttributes()');

        $wq = new Webquery();
        $value = "newhost";
        $wq->setHost($value);
        $this->assertEquals($value, $wq->getHost(), "set/get attribute host");

        $value = "newservice";
        $wq->setService($value);
        $this->assertEquals($value, $wq->getService(), "set/get attribute service");

        $value = "newsuffix";
        $wq->setSuffix($value);
        $this->assertEquals($value, $wq->getSuffix(), "set/get attribute suffix");

        $this->assertEquals("newservice", $wq->getRecordName(), "get attribute record_name if not yet set");
        $value = "newrecord";
        $wq->setRecordName($value);
        $this->assertEquals($value, $wq->getRecordName(), "set/get attribute record_name");

        $this->assertFalse($wq->isDryRun(), "is dryrun mode, dryrun not set");
        $value = "<status>OK</status>";
        $wq->setDryRun($value);
        $this->assertEquals($value, $wq->getDryRun(), "set/get attribute dryrun");
        $this->assertTrue($wq->isDryRun(), "is dryrun mode, dryrun is set");

        $attribs = [
            "host" => "newhost",
            "service" => "newservice",
            "suffix" => "newsuffix",
            "record_name" => "newrecord",
            "dryrun" => "<status>OK</status>",
        ];
        $this->assertEquals($attribs, $wq->getAttributes(), 'attributes after set_*');
    }


    public function testGetQueryString()
    {
        $tests = [
            ["qry" => "", "exp" => "", "ofs" => null, "max" => null, "msg" => "empty query string"],
            ["qry" => "a=1&b=2", "exp" => "a=1&b=2", "ofs" => null, "max" => null, "msg" => "simple query string"],
            ["qry" => "a=1&b=2", "exp" => "wq_ofs=3&a=1&b=2", "ofs" => 3, "max" => null, "msg" => "simple query string and wq_ofs"],
            ["qry" => "a=1&b=2", "exp" => "wq_ofs=3&wq_max=4&a=1&b=2", "ofs" => 3, "max" => 4, "msg" => "simple query string and wq_ofs and wq_max"],
            ["qry" => ["a" => 1], "exp" => "a=1", "ofs" => null, "max" => null, "msg" => "hash with 1 key/val pair"],
            ["qry" => ["a" => 1, "b" => "2"], "exp" => "wq_ofs=3&wq_max=4&a=1&b=2", "ofs" => 3, "max" => 4, "msg" => "hash with 2 key/val pairs and wq_ofs and wq_max"],
            ["qry" => ["a" => ["11", 12, "13"]], "exp" => "a=11&a=12&a=13", "ofs" => null, "max" => null, "msg" => "1 key/value pair, value is array with 3 values"],
            ["qry" => ["c" => "c1", "a" => ["11", 12], "b" => "b2"], "exp" => "c=c1&a=11&a=12&b=b2", "ofs" => null, "max" => null, "msg" => "3 key/value pairs, 2nd value is array with 2 values"],
            ["qry" => ["a" => "1", "!b" => "2"], "exp" => "a=1&wq_rel=AND+NOT&b=2", "ofs" => null, "max" => null, "msg" => "2 key/value pairs, second key has NOT prefix"],
            ["qry" => ["a"  => "1", "!b" => "2", "c"  => "3"], "exp" => "a=1&wq_rel=AND+NOT&b=2&c=3", "ofs" => null, "max" => null, "msg" => "3 key/value pairs, 2nd key has NOT prefix"],
            ["qry" => ["a"  => "1", "!b" => "2", "!c"  => "3"], "exp" => "a=1&wq_rel=AND+NOT&b=2&wq_rel=AND+NOT&c=3", "ofs" => null, "max" => null, "msg" => "3 key/value pairs, 2nd and 3rd key have NOT prefix"],
            ["qry" => ["c" => "3", "!a" => ["11", 12, "13"], "d" => "4"], "exp" => "c=3&wq_rel=AND+NOT&wq_par=open&a=11&a=12&a=13&wq_par=close&d=4", "ofs" => null, "max" => null, "msg" => "3 key/value pairs, 2nd value is array and has NOT prefix"],
            ["qry" => ["status" => "wacht op lener", "a" => "1"], "exp" => "status=wacht%20op%20lener&a=1", "ofs" => null, "max" => null, "msg" => "2 key/value pairs, value must be urlencoded"],
            ["qry" => ["status" => ["wacht op lener", "wacht op item"]], "exp" => "status=wacht%20op%20lener&status=wacht%20op%20item", "ofs" => null, "max" => null, "msg" => "1 key/value pair, value is array with 2 values which must be urlencoded"],
        ];

        $t = 0;
        foreach ($tests as $test) {
            $result = \Library\Webquery::getQueryString($test["qry"], $test["ofs"], $test["max"]);
            $this->assertEquals($test["exp"], $result, "$t: " . $test["msg"]);
            $t++;
        }
    }


    public function testGetSearchUrl()
    {
        // with default settings
        $wq = new Webquery();
        $expect = 'http://backend.library.wur.nl/WebQuery//xml';
        $this->assertEquals($expect, $wq->getSearchUrl(), 'no properties set, default url');

        // with url properties set
        $wq = new Webquery();
        $wq->setHost('devel.library.wur.nl')
            ->setService('testservice')
            ->setSuffix('record');
        $expect = 'http://devel.library.wur.nl/WebQuery/testservice/record';
        $this->assertEquals($expect, $wq->getSearchUrl(), 'url properties set');

        // with isn set
        $wq = new Webquery();
        $wq->setHost('devel.library.wur.nl')
            ->setService('testservice')
            ->setSuffix('record')
            ->setIsn("123");
        $expect = 'http://devel.library.wur.nl/WebQuery/testservice/record/123';
        $this->assertEquals($expect, $wq->getSearchUrl(), 'url properties set');

        // with query string set
        $wq = new Webquery();
        $wq->setHost('devel.library.wur.nl')
            ->setService('anotherservice')
            ->setQuery('q=*');
        $expect = 'http://devel.library.wur.nl/WebQuery/anotherservice/xml?q=*';
        $this->assertEquals($expect, $wq->getSearchUrl(), 'query string set');

        // with query array set
        $wq = new Webquery();
        $wq->setService('aservice')
            ->setQuery(["first" => "1", "second" => "2"]);
        $expect = 'http://backend.library.wur.nl/WebQuery/aservice/xml?first=1&second=2';
        $this->assertEquals($expect, $wq->getSearchUrl(), 'query array set');

        // with query array set
        $wq = new Webquery();
        $wq->setService('aservice')
            ->setQuery("q=*")
            ->setWqOfs(10)
            ->setWqMax(50);
        $expect = 'http://backend.library.wur.nl/WebQuery/aservice/xml?wq_ofs=10&wq_max=50&q=*';
        $this->assertEquals($expect, $wq->getSearchUrl(), 'wq_ofs and wq_max set');
    }


    public function testGetNewUrl()
    {
        // with default settings
        $wq = new Webquery();
        $expect = 'http://backend.library.wur.nl/WebQuery//new_xml';
        $this->assertEquals($expect, $wq->getNewUrl(), 'no properties set, default url');

        // with url properties set
        $wq = new Webquery();
        $wq->setHost('devel.library.wur.nl')
            ->setService('testservice')
             ->setSuffix('record');
        $expect = 'http://devel.library.wur.nl/WebQuery/testservice/new_record';
        $this->assertEquals($expect, $wq->getNewUrl(), 'url properties set, default url');
    }


    public function testGetUpdateUrl()
    {
        // with default settings
        $wq = new Webquery();
        $expect = 'http://backend.library.wur.nl/WebQuery//update_xml/';
        $this->assertEquals($expect, $wq->getUpdateUrl(), 'no properties set, default url');

        // with url properties set
        $wq = new Webquery();
        $wq->setHost('devel.library.wur.nl')
            ->setService('testservice')
            ->setSuffix('record')
            ->setIsn("13");
        $expect = 'http://devel.library.wur.nl/WebQuery/testservice/update_record/13';
        $this->assertEquals($expect, $wq->getUpdateUrl(), 'url properties set, default url');
    }


    /**
     * @dataProvider dataGetError
     */
    public function testGetError($resp, $expect, $comment)
    {
        // with default settings
        $wq = new Webquery();
        $wq->setHttpResponse($resp);
        $this->assertEquals($expect, $wq->getError(), $comment);
    }

    public function dataGetError()
    {
        $r1 = new \GuzzleHttp\Psr7\Response("200", ["Content-Type" => "text/xml"], "<root><data>12345</data></root>");
        $x2 = "<set><error code='WQW_NO_HITS' status='200'><message>no hits</message></error></set>";
        $r2 = new \GuzzleHttp\Psr7\Response("404", ["Content-Type" => "text/xml"], $x2);
        $e2 = ["code" => "WQW_NO_HITS", "status" => "200", "message" => "no hits"];
        return [
            [null, null, "no response set (default)"],
            [$r1, null, "valid xml response"],
            [$r2, $e2, "WQW_NO_HITS response"],
        ];
    }


    /**
     * @dataProvider dataGetErrorValue
     */
    public function testGetErrorValue($resp, $name, $expect, $comment)
    {
        // with default settings
        $wq = new Webquery();
        $wq->setHttpResponse($resp);
        $this->assertEquals($expect, $wq->getErrorValue($name), $comment);
    }

    public function dataGetErrorValue()
    {
        $r1 = new \GuzzleHttp\Psr7\Response("200", ["Content-Type" => "text/xml"], "<root><data>12345</data></root>");
        $x2 = "<set><error code='WQW_NO_HITS' status='200'><message>no hits</message></error></set>";
        $r2 = new \GuzzleHttp\Psr7\Response("404", ["Content-Type" => "text/xml"], $x2);
        return [
            [null, "code", "", "no response set (default)"],
            [$r1, "code", "", "valid xml response"],
            [$r2, "code", "WQW_NO_HITS", "error response: WQW_NO_HITS"],
            [$r2, "status", "200", "error response: status"],
            [$r2, "message", "no hits", "error response: message"],
            [$r2, "statuscode", "", "error response: not existing name"],
        ];
    }


    /**
     * @dataProvider dataIsSuccess
     */
    public function testIsSuccess($resp, $expect, $comment)
    {
        // with default settings
        $wq = new Webquery();
        $wq->setHttpResponse($resp);
        $this->assertEquals($expect, $wq->isSuccess(), $comment);
    }

    public function dataIsSuccess()
    {
        $r1 = new \GuzzleHttp\Psr7\Response("200", ["Content-Type" => "text/xml"], "<root><data>12345</data></root>");
        $x2 = "<set><error code='WQW_NO_HITS' status='200'><message>no hits</message></error></set>";
        $r2 = new \GuzzleHttp\Psr7\Response("200", ["Content-Type" => "text/xml"], $x2);
        $x3 = "<set><error code='WQW_NO_HITS' status='200'><message>no hits</message></error></set>";
        $r3 = new \GuzzleHttp\Psr7\Response("404", ["Content-Type" => "text/xml"], $x3);
        $x4 = "<set><error code='WQE_FATAL' status='200'><message>big fail</message></error></set>";
        $r4 = new \GuzzleHttp\Psr7\Response("200", ["Content-Type" => "text/xml"], $x4);
        $x5 = "<set><error code='WQE_FATAL' status='500'><message>big fail</message></error></set>";
        $r5 = new \GuzzleHttp\Psr7\Response("500", ["Content-Type" => "text/xml"], $x5);
        return [
            [null, false, "no response set (default)"],
            [$r1, true, "valid xml response"],
            [$r2, true, "WQW_NO_HITS response"],
            [$r3, false, "404 response"],
            [$r4, false, "200 response with WQE_FATAL"],
            [$r5, false, "500 response with WQE_FATAL"],
        ];
    }


    public function testSearch()
    {
        // create mock http client that stores requests and responses
        $request_history = [];
        $record_xml = <<<EOT
<record isn='13' crc='123abcde' cu='tester'>
    <sub1>subval1</sub1>
    <sub2>
        <child1>childval1</child1>
        <child2>childval2</child2>
    </sub2>
    <sub3>subval 3</sub3>
</record>
EOT;
        $resp1 = "<recordset><hits>1</hits>$record_xml<next wq_ofs='1' wq_max='1' /></recordset>";
        $resp2 = '<recordset><error code="WQW_NO_HITS"><message>nothing found</message></error></recordset>';
        $headers = ["Content-Type" => "text/xml"];
        $responses = [
            new Response(200, $headers, $resp1),
            new Response(404, $headers, $resp2),
        ];
        $mock_httpclient = getMockHttpClient($responses, $request_history);

        $wq = new Webquery();
        $wq->setHttpClient($mock_httpclient);
        //$wq->set_logger(new \Monolog\Logger('search'));

        $wq->setHost('devel.library.wur.nl')
            ->setService('testservice')
            ->setRecordName("record")
            ->setQuery('record=*');

        // first request succeeds
        $result = $wq->search();
        $this->assertEquals($wq, $result, '1st: search returns self');
        $this->assertEquals('200', $wq->getHttpStatus(), '1st: response http status');
        $resp_body = $wq->getHttpResponse()->getBody();
        $this->assertEquals($resp1, $resp_body, '1st: search response body');
        $resp_xml = $wq->getHttpResponseXml();
        $expect = simplexml_load_string($resp_body);
        $this->assertEquals($expect, $resp_xml, '1st: search response xml');
        $record = $wq->getRecord();
        $expect = simplexml_load_string($record_xml);
        $this->assertEquals($expect, $record, '1st: search response record');
        $records = $wq->getRecords();
        $expect = [$expect];  // array with 1 record
        $this->assertEquals($expect, $records, '1st: search response records');
        $this->assertEquals('', $wq->getErrorCode(), '1st: search response error code');
        $this->assertEquals('', $wq->getErrorMessage(), '1st: search response error message');
        $this->assertEquals(1, $wq->getHits(), '1st: search response hits');
        $this->assertTrue($wq->hasNext(), '1st: search response has next element');
        $this->assertEquals(1, $wq->getNextOfs(), '1st: search response next wq_ofs');

        // second request fails
        $wq->setQuery('record=nothing');
        $result = $wq->search();
        $this->assertEquals($wq, $result, '2nd: search returns self');
        $this->assertEquals('404', $wq->getHttpStatus(), '2nd: response http status');
        $resp_body = $wq->getHttpResponse()->getBody();
        $this->assertEquals($resp2, $resp_body, '2nd: search response body');
        $resp_xml = $wq->getHttpResponseXml();
        $expect = simplexml_load_string($resp_body);
        $this->assertEquals($expect, $resp_xml, '2nd: search response xml');
        $record = $wq->getRecord();
        $expect = false;
        $this->assertEquals($expect, $record, '2nd: search response record');
        $records = $wq->getRecords();
        $expect = [];
        $this->assertEquals($expect, $records, '2nd: search response records');
        $this->assertEquals('WQW_NO_HITS', $wq->getErrorCode(), '2nd: search response error code');
        $this->assertEquals('nothing found', $wq->getErrorMessage(), '2nd: search response error message');
        $this->assertEquals(false, $wq->getHits(), '2nd: search response hits');
        $this->assertFalse($wq->hasNext(), '1st: search response has next element');
        $this->assertEquals(false, $wq->getNextOfs(), '2nd: search response next wq_ofs');

        $this->assertEquals(2, count($request_history), 'request history count');
        // check first request
        $request = $request_history[0]['request'];
        $this->assertEquals('GET', $request->getMethod(), 'request 1 method');
        $expect = 'http://devel.library.wur.nl/WebQuery/testservice/xml?record=*';
        $this->assertEquals($expect, $request->getUri(), 'request 1 url');
        // check second request
        $request = $request_history[1]['request'];
        $this->assertEquals('GET', $request->getMethod(), 'request 2 method');
        $expect = 'http://devel.library.wur.nl/WebQuery/testservice/xml?record=nothing';
        $this->assertEquals($expect, $request->getUri(), 'request 2 url');
    }

    public function testSearchAllRecords()
    {
        // create mock http client that stores requests and responses
        $request_history = [];
        $record1 = "<record isn='1' crc='11111111'><seq>1</seq></record>";
        $record2 = "<record isn='2' crc='22222222'><seq>2</seq></record>";
        $record3 = "<record isn='3' crc='33333333'><seq>3</seq></record>";
        $record4 = "<record isn='4' crc='44444444'><seq>4</seq></record>";
        $record5 = "<record isn='5' crc='55555555'><seq>5</seq></record>";
        $resp1 = "<recordset><hits>2</hits>${record1}${record2}<next wq_ofs='2' wq_max='2' /></recordset>";
        $resp2 = "<recordset><hits>2</hits>${record3}${record4}<next wq_ofs='4' wq_max='2' /></recordset>";
        $resp3 = "<recordset><hits>1</hits>$record5</recordset>";
        $resp4 = '<recordset><error code="WQW_NO_HITS"><message>nothing found</message></error></recordset>';
        $headers = ["Content-Type" => "text/xml"];
        $responses = [
            new Response(200, $headers, $resp1),
            new Response(200, $headers, $resp2),
            new Response(200, $headers, $resp3),
            new Response(404, $headers, $resp4),
        ];
        $mock_httpclient = getMockHttpClient($responses, $request_history);

        $wq = new Webquery();
        $wq->setHttpClient($mock_httpclient);
        //$wq->set_logger(new \Monolog\Logger('searchAllRecords'));

        $wq->setHost('devel.library.wur.nl')
            ->setService('test')
            ->setRecordName("record")
            ->setWqMax(2)
            ->setQuery('record=*');

        // search request succeeds
        $result = $wq->searchAllRecords();
        $this->assertEquals(5, count($result), 'number of records returned');
        $this->assertEquals('200', $wq->getHttpStatus(), 'response http status last request');

        // check request history
        $this->assertEquals(3, count($request_history), 'request history count');
        // check first request
        $request = $request_history[0]['request'];
        $this->assertEquals('GET', $request->getMethod(), 'request 1 method');
        $expect = 'http://devel.library.wur.nl/WebQuery/test/xml?wq_max=2&record=*';
        $this->assertEquals($expect, (string) $request->getUri(), 'request 1 url');
        // check second request
        $request = $request_history[1]['request'];
        $this->assertEquals('GET', $request->getMethod(), 'request 2 method');
        $expect = 'http://devel.library.wur.nl/WebQuery/test/xml?wq_ofs=2&wq_max=2&record=*';
        $this->assertEquals($expect, (string) $request->getUri(), 'request 2 url');
        // check third request
        $request = $request_history[2]['request'];
        $this->assertEquals('GET', $request->getMethod(), 'request 3 method');
        $expect = 'http://devel.library.wur.nl/WebQuery/test/xml?wq_ofs=4&wq_max=2&record=*';
        $this->assertEquals($expect, (string) $request->getUri(), 'request 3 url');
    }


    public function testXmlToWwwform()
    {
        $xml = getTestXml();
        $wq = new Webquery();
        $wwwform = $wq->xmlToWwwform($xml);
        $expect = 'sub1=subval1&sub2=&child1=childval1&child2=childval2&sub3=subval+3';
        $this->assertEquals($expect, $wwwform, 'record in x-www-form-urlencoded string');
    }

    public function testWwwFormToFormdata()
    {
        $wwwform = getTestWwwForm();
        $wq = new Webquery();
        $formdata = $wq->wwwFormToFormdata($wwwform);
        $expect = [
            [
                'name' => 'sub1',
                'contents' => 'subval1'
            ],
            [
                'name' => 'sub2',
                'contents' => ''
            ],
            [
                'name' => 'child1',
                'contents' => 'childval1'
            ],
            [
                'name' => 'child2',
                'contents' => 'childval2'
            ],
            [
                'name' => 'sub3',
                'contents' => 'subval+3'
            ]
        ];

        $this->assertEquals($expect, $formdata, 'record in form data');
    }


    public function testCreateRecord()
    {
        // create mock http client that stores requests and responses
        $request_history = [];
        $headers = ["Content-Type" => "text/xml"];
        $responses = [
            new Response(200, $headers, '<recordset><error status="200" isn="13"><code>WQW_UPDATE_OK</code><message>record created</message></error></recordset>'),
            new Response(500, $headers, '<recordset><error status="501"><code>WQE_FAILED</code><message>record not created</message></error></recordset>'),
            new Response(200, $headers, '<recordset><error status="200" isn="14"><code>WQW_UPDATE_OK</code><message>record created</message></error></recordset>'),
        ];
        $mock_httpclient = getMockHttpClient($responses, $request_history);

        // Create a testfile for fileupload in /tmp
        $tmpfname = tempnam("/tmp", "HttpClientTraitTest");
        $handle = fopen($tmpfname, "w");
        fwrite($handle, "contents tempfile");
        fclose($handle);

        $test_field_name = 'filenaam';

        $wq = new Webquery();
        $wq->setHttpClient($mock_httpclient);
        //$wq->set_logger(new \Monolog\Logger('createRecord'));

        $wq->setHost('devel.library.wur.nl')
            ->setService('testservice');

        $xml = getTestXml();

        // first request succeeds
        $result = $wq->createRecord($xml);
        $this->assertTrue($result, 'first request return value');
        $this->assertEquals('200', $wq->getErrorStatus(), 'first response error status');
        $this->assertEquals('WQW_UPDATE_OK', $wq->getErrorCode(), 'first response error code');
        $this->assertEquals('record created', $wq->getErrorMessage(), 'first response error message');
        $this->assertEquals('13', $wq->getNewIsn(), 'first response isn');

        // second request fails
        $wq->setSuffix('record');
        $result = $wq->createRecord($xml);
        $this->assertFalse($result, 'second request return value');
        $this->assertEquals('501', $wq->getErrorStatus(), 'second response error status');
        $this->assertEquals('WQE_FAILED', $wq->getErrorCode(), 'second response error code');
        $this->assertEquals('record not created', $wq->getErrorMessage(), 'second response error message');
        $this->assertEquals('', $wq->getNewIsn(), 'second response isn');

        // third request (multipart) succeeds
        $result = $wq->createRecord($xml, $tmpfname, $test_field_name);
        $this->assertTrue($result, 'third request return value');
        $this->assertEquals('200', $wq->getErrorStatus(), 'third response error status');
        $this->assertEquals('WQW_UPDATE_OK', $wq->getErrorCode(), 'third response error code');
        $this->assertEquals('record created', $wq->getErrorMessage(), 'third response error message');
        $this->assertEquals('14', $wq->getNewIsn(), 'third response isn');

        $this->assertEquals(3, count($request_history), 'request history count');
        // check first request
        $request = $request_history[0]['request'];
        $this->assertEquals('POST', $request->getMethod(), 'request 1 method');
        $expect = 'http://devel.library.wur.nl/WebQuery/testservice/new_xml';
        $this->assertEquals($expect, $request->getUri(), 'request 1 url');
        $expect = 'sub1=subval1&sub2=&child1=childval1&child2=childval2&sub3=subval+3';
        $this->assertEquals($expect, $request->getBody(), 'request 1 body');
        // check second request
        $request = $request_history[1]['request'];
        $this->assertEquals('POST', $request->getMethod(), 'request 2 method');
        $expect = 'http://devel.library.wur.nl/WebQuery/testservice/new_record';
        $this->assertEquals($expect, $request->getUri(), 'request 2 url');
        $expect = 'sub1=subval1&sub2=&child1=childval1&child2=childval2&sub3=subval+3';
        $this->assertEquals($expect, $request->getBody(), 'request 2 body');
        // check second request
        $request = $request_history[2]['request'];
        $this->assertEquals('POST', $request->getMethod(), 'request 3 method');
        $expect = 'http://devel.library.wur.nl/WebQuery/testservice/new_record';
        $this->assertEquals($expect, $request->getUri(), 'request 3 url');
        $expect = getTestMultipartNewHttpMessage($request->getBody()->getBoundary(), $tmpfname, $test_field_name);
        $this->assertEquals($expect, $request->getBody()->getContents(), 'request 3 body');
        unlink($tmpfname);
    }


    public function testUpdateRecord()
    {
        // create mock http client that stores requests and responses
        $request_history = [];

        // Create a testfile for fileupload in /tmp
        $tmpfname = tempnam("/tmp", "HttpClientTraitTest");
        $handle = fopen($tmpfname, "w");
        fwrite($handle, "contents tempfile");
        fclose($handle);

        $test_field_name = 'filenaam';

        $record_13 = "<record isn='13' crc='123abcde' cu='tester'><a>0</a><a>1</a><b>2</b></record>";
        $record_14 = "<record isn='14' crc='123abcde' cu='tester'><a>0</a><a>1</a><b>2</b><$test_field_name>$tmpfname</$test_field_name></record>";
        $resp_13 = "<recordset><error status='200' isn='13'><code>WQW_UPDATE_OK</code><message>record updated</message></error>$record_13</recordset>";
        $resp_14 = "<recordset><error status='200' isn='14'><code>WQW_UPDATE_OK</code><message>record updated</message></error>$record_14</recordset>";
        $resp_err = "<recordset><error status='501'><code>WQE_FAILED</code><message>record not updated</message></error></recordset>";
        $headers = ["Content-Type" => "text/xml"];
        $responses = [
            new Response(200, $headers, $resp_13),
            new Response(500, $headers, $resp_err),
            new Response(200, $headers, $resp_14),
        ];
        $mock_httpclient = getMockHttpClient($responses, $request_history);

        $wq = new Webquery();
        $wq->setHttpClient($mock_httpclient);
        //$wq->set_logger(new \Monolog\Logger('createRecord'));

        $wq->setHost('devel.library.wur.nl')
            ->setService('testservice');

        $data = "a=&a=1&b=2";

        // first request succeeds
        $result = $wq->setIsn("13")->updateRecord($data);
        $this->assertTrue($result, 'first request return value');
        $this->assertEquals('200', $wq->getErrorStatus(), 'first response error status');
        $this->assertEquals('WQW_UPDATE_OK', $wq->getErrorCode(), 'first response error code');
        $this->assertEquals('record updated', $wq->getErrorMessage(), 'first response error message');
        $this->assertEquals('13', $wq->getNewIsn(), 'first response isn');

        // second request fails
        $wq->setSuffix('record')->setIsn("14");
        $result = $wq->updateRecord("$data&c=3");
        $this->assertFalse($result, 'second request return value');
        $this->assertEquals('501', $wq->getErrorStatus(), 'second response error status');
        $this->assertEquals('WQE_FAILED', $wq->getErrorCode(), 'second response error code');
        $this->assertEquals('record not updated', $wq->getErrorMessage(), 'second response error message');
        $this->assertEquals('', $wq->getNewIsn(), 'second response isn');

        // third request (multipart) succeeds
        $result = $wq->setSuffix('xml')->setIsn("14")->updateRecord($data, false, $tmpfname, $test_field_name);
        $this->assertTrue($result, 'third request return value');
        $this->assertEquals('200', $wq->getErrorStatus(), 'third response error status');
        $this->assertEquals('WQW_UPDATE_OK', $wq->getErrorCode(), 'third response error code');
        $this->assertEquals('record updated', $wq->getErrorMessage(), 'third response error message');
        $this->assertEquals('14', $wq->getNewIsn(), 'third response isn');

        $this->assertEquals(3, count($request_history), 'request history count');
        // check first request
        $request = $request_history[0]['request'];
        $this->assertEquals('POST', $request->getMethod(), 'request 1 method');
        $expect = 'http://devel.library.wur.nl/WebQuery/testservice/update_xml/13';
        $this->assertEquals($expect, $request->getUri(), 'request 1 url');
        $this->assertEquals($data, $request->getBody(), 'request 1 body');
        // check second request
        $request = $request_history[1]['request'];
        $this->assertEquals('POST', $request->getMethod(), 'request 2 method');
        $expect = 'http://devel.library.wur.nl/WebQuery/testservice/update_record/14';
        $this->assertEquals($expect, $request->getUri(), 'request 2 url');
        $this->assertEquals("$data&c=3", $request->getBody(), 'request 2 body');
        // check third request
        $request = $request_history[2]['request'];
        $this->assertEquals('POST', $request->getMethod(), 'request 3 method');
        $expect = 'http://devel.library.wur.nl/WebQuery/testservice/update_xml/14';
        $this->assertEquals($expect, $request->getUri(), 'request 3 url');
        $expect = getTestMultipartUpdateHttpMessage($request->getBody()->getBoundary(), $tmpfname, $test_field_name);
        $this->assertEquals($expect, $request->getBody()->getContents(), 'request 3 body');
        unlink($tmpfname);
    }

    /**
     * test with simplexml as data
     */
    public function testUpdateRecords()
    {
        // initiate the mock http client.
        // these variables will be send back in queue (of 1 in this case) when we send a request
        $request_history = [];
        $headers = ["Content-Type" => "text/xml"];
        $resp = "<wmspersona-set><error status='200' isn='23143'><code>WQW_UPDATE_OK</code><message>record updated</message></error><record><a>0</a></record></wmspersona-set>";
        $responses = [
            new Response(200, $headers, $resp),
        ];
        $mock_httpclient = getMockHttpClient($responses, $request_history);

        // instantiate WQ object with the mock client
        $wq = new Webquery();
        $wq->setHttpClient($mock_httpclient);
        $wq->setHost('devel.library.wur.nl')
            ->setService('testservice');

        $dataXml = getTestXml2();
        $result = $wq->setIsn("23143")->updateRecord($dataXml);
        $this->assertTrue($result, 'first request return value');
        $this->assertEquals('200', $wq->getErrorStatus(), 'first response error status');
        $this->assertEquals('WQW_UPDATE_OK', $wq->getErrorCode(), 'first response error code');
        $this->assertEquals('record updated', $wq->getErrorMessage(), 'first response error message');
        $this->assertEquals('23143', $wq->getNewIsn(), 'first response isn');
    }

    public function testDryRun()
    {
        $wq = new Webquery();
        //$wq->set_logger(new \Monolog\Logger('dryrun'));
        $dryrun_resp = "<test-set><hits>1</hits><test>OK</test><test-set>";
        $wq->setAttributes([
                "host" => "localhost",
                "service" => "test",
                "dryrun" => $dryrun_resp,
            ])
            ->setQuery('test=*');

        // test post
        $result = $wq->setIsn("1")->httpPost($wq->getUpdateUrl(), "post=data");
        $this->assertEquals('200', $result->getStatusCode(), 'http get response http status');
        $this->assertEquals($dryrun_resp, $result->getBody(), 'http get response body');
    }

    public function testStringyfyXML()
    {
        // TODO: add tests for parameter $skipFirstElement
        $respString = 'wmspersona=&header=&description=description&body=&persona=&oclcExpirationDate=2019-04-14T00:00:01Z';
        $sourceXML = getTestXml2();
        $wq = new Webquery();
        $response = $wq->stringifyXml($sourceXML, false);
        $this->assertEquals($respString, $response, 'stringyfyXML std function');
        // cant try-catch a typeerror
        $this->expectException(\TypeError::class);
        $wq->stringifyXml("123");
    }
}


/***********  Helper Functions    **************/

function getTestXml()
{
    $xml_txt = <<<EOT
<record>
    <sub1>subval1</sub1>
    <sub2>
        <child1>childval1</child1>
        <child2>childval2</child2>
    </sub2>
    <sub3>subval 3</sub3>
</record>
EOT;
    return simplexml_load_string($xml_txt);
}

function getTestXml2()
{
    $xml_txt = <<<EOT
<wmspersona-set>
     <!-- cardEnddate: 2019-04-14 - cardStatus: -P11D -->
    <wmspersona isn="23143" crc="cac61880">
        <header>
            <description>
            description
            </description>
        </header>

        <body>
            <persona>
            <oclcExpirationDate>2019-04-14T00:00:01Z</oclcExpirationDate>
            </persona>
        </body>
    </wmspersona>
</wmspersona-set>
EOT;
    return simplexml_load_string($xml_txt);
}

function getTestWwwForm(): string
{
    return 'sub1=subval1&sub2=&child1=childval1&child2=childval2&sub3=subval+3';
}

function getTestMultipartNewHttpMessage($boundary, $file_name, $file_field): string
{
    $file_name = basename($file_name);

    $format = <<<EOT
--$boundary\r\n
Content-Disposition: form-data; name="sub1"\r\n
Content-Length: 7\r\n
\r\n
subval1\r\n
--$boundary\r\n
Content-Disposition: form-data; name="sub2"\r\n
\r\n
\r\n
--$boundary\r\n
Content-Disposition: form-data; name="child1"\r\n
Content-Length: 9\r\n
\r\n
childval1\r\n
--$boundary\r\n
Content-Disposition: form-data; name="child2"\r\n
Content-Length: 9\r\n
\r\n
childval2\r\n
--$boundary\r\n
Content-Disposition: form-data; name="sub3"\r\n
Content-Length: 8\r\n
\r\n
subval+3\r\n
--$boundary\r\n
Content-Disposition: form-data; name="$file_field"; filename="$file_name"\r\n
Content-Length: 17\r\n
\r\n
contents tempfile\r\n
--$boundary--\r\n
EOT;
    return implode("\n", array_filter(explode("\n", $format))) . "\n";
}

function getTestMultipartUpdateHttpMessage($boundary, $file_name, $file_field): string
{
    $file_name = basename($file_name);

    $format = <<<EOT
--$boundary\r\n
Content-Disposition: form-data; name="a"\r\n
\r\n
\r\n
--$boundary\r\n
Content-Disposition: form-data; name="a"\r\n
Content-Length: 1\r\n
\r\n
1\r\n
--$boundary\r\n
Content-Disposition: form-data; name="b"\r\n
Content-Length: 1\r\n
\r\n
2\r\n
--$boundary\r\n
Content-Disposition: form-data; name="$file_field"; filename="$file_name"\r\n
Content-Length: 17\r\n
\r\n
contents tempfile\r\n
--$boundary--\r\n
EOT;
    return implode("\n", array_filter(explode("\n", $format))) . "\n";
}
