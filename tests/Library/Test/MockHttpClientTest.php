<?php

// phpcs:disable Generic.Files.LineLength

namespace Tests\Library\Test;

use Library\Test\MockHttpClient;

class MockHttpClientTest extends \PHPUnit\Framework\TestCase
{
    private static $test_data_dir = __DIR__ . "/data";


    public function testGetSetDataDir()
    {
        $this->assertEquals(".", MockHttpClient::getDataDir(), "default DataDir value");

        MockHttpClient::setDataDir(self::$test_data_dir);
        $this->assertEquals(self::$test_data_dir, MockHttpClient::getDataDir(), "test DataDir value set");

        // reset DataDir value
        MockHttpClient::setDataDir(".");
    }

    public function testConstructResponses()
    {
        // set DataDir value
        MockHttpClient::setDataDir(self::$test_data_dir);

        $result = MockHttpClient::constructResponses([]);
        $this->assertEquals(0, count($result), "empty array: number of responses");

        $resp401 = new \GuzzleHttp\Psr7\Response(401);
        $result = MockHttpClient::constructResponses([$resp401]);
        $this->assertEquals(1, count($result), "resp401: number of responses");
        $this->assertEquals($resp401, $result[0], "resp401: result contains original response");

        $responses = [
            ["body" => "<body>1</body>"],
            ["headers" => ["Content-Type" => "text/text"], "file" => "file-result-1.txt"],
            ["status" => 500, "body" => "<error>500</error>"],
        ];
        $result = MockHttpClient::constructResponses($responses);
        $this->assertEquals(3, count($result), "array: number of responses");

        $resp = $result[0];
        $this->assertEquals(200, $resp->getStatusCode(), "array[0]: response status");
        $this->assertEquals("text/xml", $resp->getHeaderLine("Content-Type"), "array[0]: response content-type");
        $this->assertEquals("<body>1</body>", $resp->getBody(), "array[0]: response body");

        $resp = $result[1];
        $this->assertEquals(200, $resp->getStatusCode(), "array[1]: response status");
        $this->assertEquals("text/text", $resp->getHeaderLine("Content-Type"), "array[1]: response content-type");
        $this->assertEquals("file result 1\n", $resp->getBody(), "array[1]: response body");

        $resp = $result[2];
        $this->assertEquals(500, $resp->getStatusCode(), "array[2]: response status");
        $this->assertEquals("text/xml", $resp->getHeaderLine("Content-Type"), "array[2]: response content-type");
        $this->assertEquals("<error>500</error>", $resp->getBody(), "array[2]: response body");

        // reset DataDir value
        MockHttpClient::setDataDir(".");
    }

    public function testAddResponses()
    {
        // set DataDir value
        MockHttpClient::setDataDir(self::$test_data_dir);

        $mhc = new MockHttpClient();
        $this->assertEquals(0, $mhc->getResponsesCount(), "number of responses after empty constructor");

        $resp401 = new \GuzzleHttp\Psr7\Response(401);
        $mhc = new MockHttpClient([$resp401]);
        $this->assertEquals(1, $mhc->getResponsesCount(), "number of responses after constructor with 1 response");

        $responses = [
            ["body" => "<body>1</body>"],
            ["headers" => ["Content-Type" => "text/text"], "file" => "file-result-1.txt"],
            ["status" => 500, "body" => "<error>500</error>"],
        ];

        $mhc = new MockHttpClient();
        $mhc->addResponses($responses);
        $this->assertEquals(3, $mhc->getResponsesCount(), "number of responses after empty constructor and 3 added");

        $resp401 = new \GuzzleHttp\Psr7\Response(401);
        $mhc = new MockHttpClient([$resp401]);
        $mhc->addResponses($responses);
        $this->assertEquals(4, $mhc->getResponsesCount(), "number of responses after constructor with 1 response and 3 added");

        // reset DataDir value
        MockHttpClient::setDataDir(".");
    }

    public function testgetHistory()
    {
        // set DataDir value
        MockHttpClient::setDataDir(self::$test_data_dir);

        $mhc = new MockHttpClient([["body" => "<body>1</body>"]]);
        $this->assertEquals(0, count($mhc->getHistory()), "number of requests in the history after 0 request");
        $resp = $mhc->get("http://localhost/test1");
        $this->assertEquals(1, count($mhc->getHistory()), "number of requests in the history after 1 request");

        $responses = [
            ["body" => "<body>1</body>"],
            ["headers" => ["Content-Type" => "text/text"], "file" => "file-result-1.txt"],
            ["status" => 500, "body" => "<error>500</error>"],
        ];
        $mhc = new MockHttpClient($responses);
        $resp = $mhc->get("http://localhost/test1");
        $resp = $mhc->get("http://localhost/test2");
        $this->assertEquals(2, count($mhc->getHistory()), "number of requests in the history after 2 request");

        // reset DataDir value
        MockHttpClient::setDataDir(".");
    }
}
