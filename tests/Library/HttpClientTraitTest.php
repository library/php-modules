<?php

// phpcs:disable Generic.Files.LineLength

namespace Tests\Library;

class HttpClientTraitTest extends \PHPUnit\Framework\TestCase
{
    public function testCreateHttpClient()
    {
        // test connection exception
        $mock_handler = new \GuzzleHttp\Handler\MockHandler(
            [
                new \GuzzleHttp\Exception\ConnectException("Error 1", new \GuzzleHttp\Psr7\Request("GET", "test")),
                new \GuzzleHttp\Psr7\Response(200),
            ]
        );
        $http_client = \Library\HttpClientTrait::createHttpClient(["max_retries" => 3], null, $mock_handler);
        $this->assertEquals(200, $http_client->request("GET", "/")->getStatusCode(), "retry on ConnectException");

        // test 500 error and delay
        $mock_handler = new \GuzzleHttp\Handler\MockHandler(
            [
                new \GuzzleHttp\Psr7\Response(500),
                new \GuzzleHttp\Psr7\Response(200),
            ]
        );
        $http_client = \Library\HttpClientTrait::createHttpClient(["max_retries" => 3, "base_delay" => 100], null, $mock_handler);
        $t1 = microtime(true);
        $this->assertEquals(200, $http_client->request("GET", "/")->getStatusCode(), "retry on 500 error");
        $t2 = microtime(true);
        $this->assertGreaterThan($t1 + 0.09, $t2, "delay at least 90 millisec (1x100)");  # exp < act
        $this->assertLessThan($t1 + 0.11, $t2, "delay less than 110 millisec (1x100)"); # exp > act

        // test handler without retry, max_retries not set
        $mock_handler = new \GuzzleHttp\Handler\MockHandler(
            [
                new \GuzzleHttp\Psr7\Response(500),
                new \GuzzleHttp\Psr7\Response(200),
            ]
        );
        $http_client = \Library\HttpClientTrait::createHttpClient([], null, $mock_handler);
        $this->assertEquals(500, $http_client->request("GET", "/", ["http_errors" => false])->getStatusCode(), "without retry handler");

        // test multiple retries on 500 error and delay
        $mock_handler = new \GuzzleHttp\Handler\MockHandler(
            [
                new \GuzzleHttp\Psr7\Response(500),
                new \GuzzleHttp\Psr7\Response(500),
                new \GuzzleHttp\Psr7\Response(500),
                new \GuzzleHttp\Psr7\Response(200),
            ]
        );
        $http_client = \Library\HttpClientTrait::createHttpClient(["max_retries" => 3, "base_delay" => 100], null, $mock_handler);
        $t1 = microtime(true);
        $this->assertEquals(200, $http_client->request("GET", "/")->getStatusCode(), "retry on 3 x 500 errors");
        $t2 = microtime(true);
        $this->assertGreaterThan($t1 + 0.69, $t2, "delay at least 690 millisec (100+200+400)");  # exp < act
        $this->assertLessThan($t1 + 0.71, $t2, "delay less than 710 millisec (100+200+400)"); # exp > act

        // test stopping when max_retries are done
        $mock_handler = new \GuzzleHttp\Handler\MockHandler(
            [
                new \GuzzleHttp\Psr7\Response(500),
                new \GuzzleHttp\Psr7\Response(500),
                new \GuzzleHttp\Psr7\Response(500),
                new \GuzzleHttp\Psr7\Response(200),
            ]
        );
        $http_client = \Library\HttpClientTrait::createHttpClient(["max_retries" => 2], null, $mock_handler);
        $this->assertEquals(500, $http_client->request("GET", "/", ["http_errors" => false])->getStatusCode(), "stop retry after 2 x 500 errors");
    }

    public function testGetSetHttpClient()
    {
        $testclient = new HttpClient();

        // no http_client set yet
        $default_client = $testclient->getHttpClient();
        $this->assertInstanceOf("\GuzzleHttp\Client", $default_client, "getHttpClient() returns instance of \GuzzleHttp\Client");

        $my_client = new \GuzzleHttp\Client();
        $result = $testclient->setHttpClient($my_client);
        $this->assertEquals($testclient, $result, "setHttpClient() returns this");
        $this->assertEquals($my_client, $testclient->getHttpClient(), "getHttpClient() returns the client that is set");

        // test default retry http client
        $testclient = new HttpClient();
        $testclient->setHttpOptions(["max_retries" => 3, "base_delay" => 100]);
        $retry_client = $testclient->getHttpClient();
        $config = $retry_client->getConfig();
        // the handlerstack string representation contains all names of the middleware
        $handler_string = (string) $config["handler"];
        $this->assertMatchesRegularExpression('/retry/', $handler_string, "handler has retry middleware");
    }

    public function testGetSetHttpHeaders()
    {
        $testclient = new HttpClient();

        // no headers set yet
        $this->assertEquals([], $testclient->getHttpHeaders(), "default getHttpHeaders() returns empty array");

        // set/get headers
        $headers = ["Content-Type" => "plain/text"];
        $this->assertEquals($testclient, $testclient->setHttpHeaders($headers), "setHttpHeaders() returns this");
        $this->assertEquals($headers, $testclient->getHttpHeaders(), "getHttpHeaders() returns set headers");

        // set/get single header
        $testclient->setHttpHeader("Accept", "application/xml");
        $this->assertEquals("application/xml", $testclient->getHttpHeader("Accept"), "setHttpHeader() sets header");
        $headers["Accept"] = "application/xml";
        $this->assertEquals($headers, $testclient->getHttpHeaders(), "getHttpHeaders() returns all headers");

        $testclient->setHttpHeader("Content-Type", "text/html");
        $headers["Content-Type"] = "text/html";
        $this->assertEquals($headers, $testclient->getHttpHeaders(), "getHttpHeaders() returns all headers");
    }

    public function testGetSetHttpOptions()
    {
        $testclient = new HttpClient();

        // no options set yet
        $this->assertEquals([], $testclient->getHttpOptions(), "default getHttpOptions() returns empty array");

        // set/get options
        $options = ["test" => 'passed'];
        $this->assertEquals($testclient, $testclient->setHttpOptions($options), "setHttpOptions() returns this");
        $this->assertEquals($options, $testclient->getHttpOptions(), "getHttpOptions() returns set headers");
    }


    public function testGetSetHttpResponse()
    {
        $testclient = new HttpClient();

        // no response set yet
        $this->assertNull($testclient->getHttpResponse(), "default getHttpResponse() returns null");

        // set/get response
        $response = new \GuzzleHttp\Psr7\Response("200", ["Content-Type" => "text/xml"], "<root><data>12345</data></root>");
        $this->assertEquals($testclient, $testclient->setHttpResponse($response), "setHttpResponse() returns this");
        $this->assertEquals($response, $testclient->getHttpResponse(), "getHttpResponse() returns set response");
    }


    public function testGetHttpStatus()
    {
        $testclient = new HttpClient();

        // no response set yet
        $this->assertEquals("", $testclient->getHttpStatus(), "default getHttpStatus() returns empty string");

        // set response to get a status
        $testclient->setHttpResponse(new \GuzzleHttp\Psr7\Response("201"));
        $this->assertEquals("201", $testclient->getHttpStatus(), "getHttpStatus() returns status 201");

        // test status when new response is set
        $testclient->setHttpResponse(new \GuzzleHttp\Psr7\Response("500"));
        $this->assertEquals("500", $testclient->getHttpStatus(), "getHttpStatus() returns status 500");
    }


    public function testGetHttpResponseXml()
    {
        $testclient = new HttpClient();

        // no response set yet
        $this->assertNull($testclient->getHttpResponseXml(), "default getHttpResponseXml() returns null");

        // set response
        $testclient->setHttpResponse(new \GuzzleHttp\Psr7\Response("200", ["Content-Type" => "text/xml"], "<root><data>12345</data></root>"));
        $xml = $testclient->getHttpResponseXml();
        $this->assertInstanceOf("\SimpleXMLElement", $xml, "getHttpResponseXml() returns instance of \SimpleXMLElement");
        $this->assertEquals("12345", $xml->data, "getHttpResponseXml() returns data from parsed response");

        // test xml when new response is set
        $testxml = "<root><test>success</test></root>";
        $testclient->setHttpResponse(new \GuzzleHttp\Psr7\Response("200", ["Content-Type" => "application/xml"], $testxml));
        $xml = $testclient->getHttpResponseXml();
        $this->assertEquals("success", $xml->test, "getHttpResponseXml() returns test from parsed response");

        // test other xml content-type
        $testclient->setHttpResponse(new \GuzzleHttp\Psr7\Response("200", ["Content-Type" => "application/atom+xml"], $testxml));
        $xml = $testclient->getHttpResponseXml();
        $this->assertEquals("success", $xml->test, "getHttpResponseXml() returns test from parsed response");

        // test another xml content-type
        $testclient->setHttpResponse(new \GuzzleHttp\Psr7\Response("200", ["Content-Type" => "application/atom+xml;content='application/vnd.oclc.marc21+xml'"], $testxml));
        $xml = $testclient->getHttpResponseXml();
        $this->assertEquals("success", $xml->test, "getHttpResponseXml() returns test from parsed response");

        // test other content-type not xml
        $testclient->setHttpResponse(new \GuzzleHttp\Psr7\Response("200", ["Content-Type" => "application/json"], $testxml));
        $xml = $testclient->getHttpResponseXml();
        $this->assertEquals("", $xml->test, "getHttpResponseXml() returns empty string for test element");
        $this->assertEquals("RESPONSE_NO_XML", $xml->error->code, "getHttpResponseXml() returns error code from parse error response");

        // test parse error from invalid xml
        $testclient->setHttpResponse(new \GuzzleHttp\Psr7\Response("200", ["Content-Type" => "application/xml"], "<error>$testxml"));
        $xml = $testclient->getHttpResponseXml();
        $this->assertEquals("", $xml->test, "getHttpResponseXml() returns empty string for test element");
        $this->assertEquals("XML_PARSE_ERROR", $xml->error->code, "getHttpResponseXml() returns error code from parse error response");
    }


    public function testResetHttpClient()
    {
        $testclient = new HttpClient();

        // set headers, options and response
        $testclient->setHttpHeaders(["Content-Type" => "plain/text"]);
        $testclient->setHttpOptions(["test" => "passed"]);
        $testclient->setHttpResponse(new \GuzzleHttp\Psr7\Response("200", ["Content-Type" => "text/xml"], "<root><data>12345</data></root>"));

        $testclient->resetHttpClient();

        // headers, options and response are reset
        $this->assertEquals([], $testclient->getHttpHeaders(), "getHttpHeaders() returns empty array");
        $this->assertEquals([], $testclient->getHttpOptions(), "getHttpOptions() returns empty array");
        $this->assertNull($testclient->getHttpResponse(), "getHttpResponse() returns null");
        $this->assertNull($testclient->getHttpResponseXml(), "getHttpResponseXml() returns null");
    }


    public function testHttpRequest()
    {
        $testclient = new HttpClient();

        $responses = [
            new \GuzzleHttp\Psr7\Response(200, ["Content-Type" => "text/xml"], "<test>response 1</test>"),
            new \GuzzleHttp\Psr7\Response(400, ["Content-Type" => "text/html"], "<html><body>response 2</body></html>"),
        ];
        $request_history = [];
        $mock_httpclient = getMockHttpClient($responses, $request_history);
        $testclient->setHttpClient($mock_httpclient);

        $response = $testclient->httpRequest("GET", "http://localhost/1", ["headers" => ["Content-Type" => "application/xml"]]);
        // check if request is correctly done
        $request = $request_history[0]['request'];
        $this->assertEquals("GET", $request->getMethod(), 'request 1 with GET method');
        $this->assertEquals("http://localhost/1", $request->getUri(), 'request 1 url');
        $this->assertEquals(["application/xml"], $request->getHeader("Content-Type"), 'request 1 content type');
        // check response
        $this->assertEquals($response, $testclient->getHttpResponse(), 'request 1 response is set in property');
        $this->assertEquals("200", $testclient->getHttpStatus(), 'request 1 response status');
        $this->assertEquals(["text/xml"], $response->getHeader("Content-Type"), 'request 1 response content type');
        $this->assertEquals("<test>response 1</test>", $response->getBody(), 'request 1 response body');

        $response = $testclient->httpRequest("POST", "http://localhost/2", ["headers" => ["Content-Type" => "text/plain"]]);
        // check if request is correctly done
        $request = $request_history[1]['request'];
        $this->assertEquals("POST", $request->getMethod(), 'request 2 with POST method');
        $this->assertEquals("http://localhost/2", $request->getUri(), 'request 2 url');
        $this->assertEquals("text/plain", $request->getHeader("Content-Type")[0], 'request 2 content type');
        // check response
        $this->assertEquals($response, $testclient->getHttpResponse(), 'request 2 response is set in property');
        // no 200 response, so we get a 500 with exception xml content
        $this->assertEquals("500", $testclient->getHttpStatus(), 'request 2 response status');
        $this->assertEquals(["text/xml"], $response->getHeader("Content-Type"), 'request 2 response content type');
        $expect = "<exception><error code=400><message>Client error: `POST http://localhost/2` resulted in a `400 Bad Request` response:\n<html><body>response 2</body></html>\n</message></error></exception>";
        $this->assertEquals($expect, (string) $response->getBody(), 'request 2 response body');
    }


    public function testGetMergedHttpOptions()
    {
        $testclient = new HttpClient();

        $options = [
            "debug" => "on",
            "headers" => ["Content-Type" => "text/xml", "X-1" => "1"],
            "timeout" => 1,
        ];
        $testclient->setHttpOptions($options);
        $result = $testclient->publicGetMergedHttpOptions([]);
        $expect = $options;
        $this->assertEquals($expect, $result, "only http_options set");

        $testclient->setHttpHeaders(["Content-Type" => "app/xml", "X-2" => "2"]);
        $result = $testclient->publicGetMergedHttpOptions([]);
        $expect["headers"] = [
            "Content-Type" => "app/xml",
            "X-1" => "1",
            "X-2" => "2",
        ];
        $this->assertEquals($expect, $result, "http_options and headers set");

        $result = $testclient->publicGetMergedHttpOptions([
            "headers" => ["Content-Type" => "text/html"],
            "timeout" => 2,
        ]);
        $expect["headers"] = [
            "Content-Type" => "text/html",
            "X-1" => "1",
            "X-2" => "2",
        ];
        $expect["timeout"] = 2;
        $this->assertEquals($expect, $result, "http_options and headers set");
    }


    public function testHttpGet()
    {
        $testclient = new HttpClient();
        //$testclient->set_logger(new \Monolog\Logger('test'));

        $responses = [
            new \GuzzleHttp\Psr7\Response(200, ["Content-Type" => "text/xml"], "<test>response 1</test>"),
            new \GuzzleHttp\Psr7\Response(200, ["Content-Type" => "text/xml"], "<test>response 2</test>"),
            new \GuzzleHttp\Psr7\Response(200, ["Content-Type" => "text/xml"], "<test>response 3</test>"),
        ];
        $request_history = [];
        $mock_httpclient = getMockHttpClient($responses, $request_history);
        $testclient->setHttpClient($mock_httpclient);

        //
        $testclient->setHttpOptions(["headers" => ["Content-Type" => "text/xml"]]);
        $response = $testclient->httpGet("http://localhost/1");
        // check if request is correctly done
        $request = $request_history[0]['request'];
        $this->assertEquals("GET", $request->getMethod(), 'request 1 with GET method');
        $this->assertEquals("http://localhost/1", $request->getUri(), 'request 1 url');
        $this->assertEquals(["text/xml"], $request->getHeader("Content-Type"), 'request 1 content type from default options');
        // check response
        $this->assertEquals($response, $testclient->getHttpResponse(), 'request 1 response is set in property');
        $this->assertEquals("<test>response 1</test>", $response->getBody(), 'request 1 response body');

        $testclient->setHttpHeaders(["Content-Type" => "application/xml"]);
        $response = $testclient->httpGet("http://localhost/2");
        // check if request is correctly done
        $request = $request_history[1]['request'];
        $this->assertEquals("GET", $request->getMethod(), 'request 2 with GET method');
        $this->assertEquals("http://localhost/2", $request->getUri(), 'request 2 url');
        $this->assertEquals(["application/xml"], $request->getHeader("Content-Type"), 'request 2 content type from default options');
        // check response
        $this->assertEquals($response, $testclient->getHttpResponse(), 'request 2 response is set in property');
        $this->assertEquals("<test>response 2</test>", $response->getBody(), 'request 2 response body');

        $response = $testclient->httpGet("http://localhost/3", ["headers" => ["Content-Type" => "last/xml"]]);
        // check if request is correctly done
        $request = $request_history[2]['request'];
        $this->assertEquals("GET", $request->getMethod(), 'request 3 with GET method');
        $this->assertEquals("http://localhost/3", $request->getUri(), 'request 3 url');
        $this->assertEquals(["last/xml"], $request->getHeader("Content-Type"), 'request 3 content type from default options');
        // check response
        $this->assertEquals($response, $testclient->getHttpResponse(), 'request 3 response is set in property');
        $this->assertEquals("<test>response 3</test>", $response->getBody(), 'request 3 response body');
    }


    public function testHttpPost()
    {
        $testclient = new HttpClient();
        //$testclient->set_logger(new \Monolog\Logger('test'));

        $responses = [
            new \GuzzleHttp\Psr7\Response(200, ["Content-Type" => "text/xml"], "<test>response 1</test>"),
            new \GuzzleHttp\Psr7\Response(200, ["Content-Type" => "text/xml"], "<test>response 2</test>"),
            new \GuzzleHttp\Psr7\Response(200, ["Content-Type" => "text/xml"], "<test>response 3</test>"),
        ];
        $request_history = [];
        $mock_httpclient = getMockHttpClient($responses, $request_history);
        $testclient->setHttpClient($mock_httpclient);

        //
        $testclient->setHttpOptions(["headers" => ["Content-Type" => "text/xml"]]);
        $response = $testclient->httpPost("http://localhost/1", "<body>1</body>");
        // check if request is correctly done
        $request = $request_history[0]['request'];
        $this->assertEquals("POST", $request->getMethod(), 'request 1 with POST method');
        $this->assertEquals("http://localhost/1", $request->getUri(), 'request 1 url');
        $this->assertEquals(["text/xml"], $request->getHeader("Content-Type"), 'request 1 content type from default options');
        $this->assertEquals("<body>1</body>", $request->getBody(), 'request 1 body');
        // check response
        $this->assertEquals($response, $testclient->getHttpResponse(), 'request 1 response is set in property');
        $this->assertEquals("<test>response 1</test>", $response->getBody(), 'request 1 response body');

        $testclient->setHttpHeaders(["Content-Type" => "application/xml"]);
        $response = $testclient->httpPost("http://localhost/2", "<body>2</body>");
        // check if request is correctly done
        $request = $request_history[1]['request'];
        $this->assertEquals("POST", $request->getMethod(), 'request 2 with POST method');
        $this->assertEquals("http://localhost/2", $request->getUri(), 'request 2 url');
        $this->assertEquals(["application/xml"], $request->getHeader("Content-Type"), 'request 2 content type from default options');
        $this->assertEquals("<body>2</body>", $request->getBody(), 'request 2 body');
        // check response
        $this->assertEquals($response, $testclient->getHttpResponse(), 'request 2 response is set in property');
        $this->assertEquals("<test>response 2</test>", $response->getBody(), 'request 2 response body');

        $response = $testclient->httpPost("http://localhost/3", "<body>3</body>", ["headers" => ["Content-Type" => "last/xml"]]);
        // check if request is correctly done
        $request = $request_history[2]['request'];
        $this->assertEquals("POST", $request->getMethod(), 'request 3 with POST method');
        $this->assertEquals("http://localhost/3", $request->getUri(), 'request 3 url');
        $this->assertEquals(["last/xml"], $request->getHeader("Content-Type"), 'request 3 content type from default options');
        $this->assertEquals("<body>3</body>", $request->getBody(), 'request 3 body');
        // check response
        $this->assertEquals($response, $testclient->getHttpResponse(), 'request 3 response is set in property');
        $this->assertEquals("<test>response 3</test>", $response->getBody(), 'request 3 response body');
    }


    public function testHttpPostMultipart()
    {
        $testclient = new HttpClient();
        //$testclient->set_logger(new \Monolog\Logger('test'));

        $responses = [
            new \GuzzleHttp\Psr7\Response(200, ["Content-Type" => "text/xml"], "<test>response 1</test>"),
            new \GuzzleHttp\Psr7\Response(200, ["Content-Type" => "text/xml"], "<test>response 2</test>"),
            new \GuzzleHttp\Psr7\Response(200, ["Content-Type" => "text/xml"], "<test>response 3</test>"),
        ];
        $request_history = [];
        $mock_httpclient = getMockHttpClient($responses, $request_history);
        $testclient->setHttpClient($mock_httpclient);

        // Create a testfile in /tmp
        $tmpfname = tempnam("/tmp", "HttpClientTraitTest");
        $handle = fopen($tmpfname, "w");
        fwrite($handle, "contents tempfile");
        fclose($handle);
        // setup multipart. Utils::tryFopen below returns a file handle
        $multipart = [
            [
               'name'     => 'foo',
               'contents' => 'data',
               'headers'  => ['X-Baz' => 'bar']
            ],
            [
               'name'     => 'baz',
               'contents' => \GuzzleHttp\Psr7\Utils::tryFopen($tmpfname, 'r')
            ],
        ];
        $response = $testclient->httpPostMultipart("http://localhost/1", $multipart);
        unlink($tmpfname);
        // check if request is correctly done
        $request = $request_history[0]['request'];
        $this->assertEquals("POST", $request->getMethod(), 'request 1, with POST method');
        $this->assertEquals("http://localhost/1", $request->getUri(), 'request 1, url');
        $headers = $request->getHeader("Content-Type");
        $this->assertEquals(1, count($headers), 'request 1, number of content type headers');
        $this->assertStringStartsWith("multipart/form-data; boundary=", $headers[0], 'request 1, multipart content type from default options');
        # if the right header is set we trust that guzzle has created a correct multipart body
        $body = (string) $request->getBody();
        $this->assertStringContainsString("\r\ndata\r\n", $body, 'request 1, multipart body part 1 data');
        $this->assertStringContainsString("\r\ncontents tempfile\r\n", $body, 'request 1, multipart body part 2 data');
        // check response
        $this->assertEquals($response, $testclient->getHttpResponse(), 'request 1 response is set in property');
        $this->assertEquals("<test>response 1</test>", $response->getBody(), 'request 1 response body');
    }


    public function testHttpPut()
    {
        $testclient = new HttpClient();
        //$testclient->set_logger(new \Monolog\Logger('test'));

        $responses = [
            new \GuzzleHttp\Psr7\Response(200, ["Content-Type" => "text/xml"], "<test>response 1</test>"),
            new \GuzzleHttp\Psr7\Response(200, ["Content-Type" => "text/xml"], "<test>response 2</test>"),
            new \GuzzleHttp\Psr7\Response(200, ["Content-Type" => "text/xml"], "<test>response 3</test>"),
        ];
        $request_history = [];
        $mock_httpclient = getMockHttpClient($responses, $request_history);
        $testclient->setHttpClient($mock_httpclient);

        //
        $testclient->setHttpOptions(["headers" => ["Content-Type" => "text/xml"]]);
        $response = $testclient->httpPut("http://localhost/1", "<body>1</body>");
        // check if request is correctly done
        $request = $request_history[0]['request'];
        $this->assertEquals("PUT", $request->getMethod(), 'request 1 with PUT method');
        $this->assertEquals("http://localhost/1", $request->getUri(), 'request 1 url');
        $this->assertEquals(["text/xml"], $request->getHeader("Content-Type"), 'request 1 content type from default options');
        $this->assertEquals("<body>1</body>", $request->getBody(), 'request 1 body');
        // check response
        $this->assertEquals($response, $testclient->getHttpResponse(), 'request 1 response is set in property');
        $this->assertEquals("<test>response 1</test>", $response->getBody(), 'request 1 response body');

        $testclient->setHttpHeaders(["Content-Type" => "application/xml"]);
        $response = $testclient->httpPut("http://localhost/2", "<body>2</body>");
        // check if request is correctly done
        $request = $request_history[1]['request'];
        $this->assertEquals("PUT", $request->getMethod(), 'request 2 with PUT method');
        $this->assertEquals("http://localhost/2", $request->getUri(), 'request 2 url');
        $this->assertEquals(["application/xml"], $request->getHeader("Content-Type"), 'request 2 content type from default options');
        $this->assertEquals("<body>2</body>", $request->getBody(), 'request 2 body');
        // check response
        $this->assertEquals($response, $testclient->getHttpResponse(), 'request 2 response is set in property');
        $this->assertEquals("<test>response 2</test>", $response->getBody(), 'request 2 response body');

        $response = $testclient->httpPut("http://localhost/3", "<body>3</body>", ["headers" => ["Content-Type" => "last/xml"]]);
        // check if request is correctly done
        $request = $request_history[2]['request'];
        $this->assertEquals("PUT", $request->getMethod(), 'request 3 with PUT method');
        $this->assertEquals("http://localhost/3", $request->getUri(), 'request 3 url');
        $this->assertEquals(["last/xml"], $request->getHeader("Content-Type"), 'request 3 content type from default options');
        $this->assertEquals("<body>3</body>", $request->getBody(), 'request 3 body');
        // check response
        $this->assertEquals($response, $testclient->getHttpResponse(), 'request 3 response is set in property');
        $this->assertEquals("<test>response 3</test>", $response->getBody(), 'request 3 response body');
    }
}


function getMockHttpClient(array $mockResponses, array &$history = null)
{
    foreach ($mockResponses as $key => $value) {
        if (is_int($value)) {
            $mockResponses[$key] = new \GuzzleHttp\Psr7\Response($value);
        }
    }
    $mock_handler = new \GuzzleHttp\Handler\MockHandler($mockResponses);
    $mock_stack = \GuzzleHttp\HandlerStack::create($mock_handler);
    if (isset($history)) {
        $mock_history = \GuzzleHttp\Middleware::history($history);
        $mock_stack->push($mock_history);
    }
    return new \GuzzleHttp\Client(['handler' => $mock_stack]);
}
