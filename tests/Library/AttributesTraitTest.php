<?php

// phpcs:disable Generic.Files.LineLength

namespace Tests\Library;

class AttributesTraitTest extends \PHPUnit\Framework\TestCase
{
    private $instance;

    protected function setUp(): void
    {
        $this->instance = new class {
            use \Library\AttributesTrait;
        };
    }

    public function testDefaultAttributes()
    {
        $this->assertEquals([], $this->instance->getAttributes(), 'default attributes');
    }

    public function testAddAttributes()
    {
        $attribs = [
            "a" => 1,
        ];
        $this->instance->addAttributes($attribs);
        $this->assertEquals($attribs, $this->instance->getAttributes(), 'first set attributes');

        $attribs = [
            "b" => 2,
            "c" => 3,
        ];
        $this->instance->addAttributes($attribs);
        $expect = [
            "a" => 1,
            "b" => 2,
            "c" => 3,
        ];
        $this->assertEquals($expect, $this->instance->getAttributes(), 'second set attributes');
    }

    public function testSetAttributes()
    {
        $attribs = [
            "a" => 1,
        ];
        $this->instance->setAttributes($attribs);
        $this->assertEquals($attribs, $this->instance->getAttributes(), 'first set attributes');

        $attribs = [
            "b" => 2,
            "c" => 3,
        ];
        $this->instance->setAttributes($attribs);
        $this->assertEquals($attribs, $this->instance->getAttributes(), 'second set attributes');
    }

    public function testSetAttribute()
    {
        $this->instance->setAttribute("a", 1);
        $expect = [
            "a" => 1,
        ];
        $this->assertEquals($expect, $this->instance->getAttributes(), 'attribute "a" set');

        $this->instance->setAttribute("b", 2);
        $expect = [
            "a" => 1,
            "b" => 2,
        ];
        $this->assertEquals($expect, $this->instance->getAttributes(), 'attribute "b" set');

        $this->instance->setAttribute("a", 3);
        $expect = [
            "a" => 3,
            "b" => 2,
        ];
        $this->assertEquals($expect, $this->instance->getAttributes(), 'attribute "a" changed');
    }

    public function testGetAttribute()
    {
        $attribs = [
            "a" => 1,
            "b" => 2,
            "c" => 3,
        ];
        $this->instance->addAttributes($attribs);

        $this->assertEquals(1, $this->instance->getAttribute("a"), 'get attribute "a" without default');
        $this->assertEquals(1, $this->instance->getAttribute("a", "x"), 'get attribute "a" with default');
        $this->assertEquals(2, $this->instance->getAttribute("b"), 'get attribute "b" without default');
        $this->assertEquals(3, $this->instance->getAttribute("c"), 'get attribute "c" without default');
        $this->assertEquals(null, $this->instance->getAttribute("d"), 'get unknown attribute "d" without default');
        $this->assertEquals("x", $this->instance->getAttribute("d", "x"), 'get unknown attribute "d" with default');
    }
}
