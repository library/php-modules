<?php

namespace Library;

/**
 * This Trait is a wrapper around the GuzzleHttp client
 * See: https://docs.guzzlephp.org
 */
trait HttpClientTrait
{
    use \Library\LoggerTrait;

    /**
     * @var \GuzzleHttp\Client psr-7 http client
     */
    protected $http_client;

    /**
     * @var array with request header settings
     */
    protected $http_headers = [];

    /**
     * @var array with http methods as keys and array with options as values
     */
    protected $http_options = [];

    /**
     * @var \GuzzleHttp\Psr7\Response
     */
    protected $http_response;

    /**
     * @var \SimpleXMLElement parsed xml from response
     */
    protected $http_response_xml;

    /**
     * create a (guzzle) http client with the default middleware stack
     * if the $http_options contains a "max_retries" setting
     * then a retry handler is added to the middleware stack
     * the "base_delay" setting specifies the base delay for the exponential delay function of the retry handler
     * if "base_delay" is not set then 1 (millisecond) is used
     *
     * @param array $http_options options for the http client
     *        see https://docs.guzzlephp.org/en/stable/request-options.html
     * @param \Psr\Log\LoggerInterface $logger logger
     * @param callable $handler HTTP handler function to use with the stack,
     *        mainly used to specify a mockhandler in tests
     * @return \GuzzleHttp\Client
     */
    public static function createHttpClient(
        array $http_options = [],
        \Psr\Log\LoggerInterface $logger = null,
        callable $handler = null
    ) {
        $stack = \GuzzleHttp\HandlerStack::create($handler);
        $max_retries = key_exists("max_retries", $http_options) ? $http_options["max_retries"] : 0;
        if ($max_retries > 0) {
            $logger = $logger ?: new \Psr\Log\NullLogger();
            $retry_decider = self::createRetryDecider($max_retries, $logger);
            $base_delay = key_exists("base_delay", $http_options) ? $http_options["base_delay"] : 1;
            $logger->debug("adding retry handler, max_retries: $max_retries, base_delay: $base_delay");
            $retry_delay = self::createRetryDelay($base_delay);
            $retry_middleware = \GuzzleHttp\Middleware::retry($retry_decider, $retry_delay);
            $stack->push($retry_middleware, "retry");
        }
        $http_options["handler"] = $stack;
        $client = new \GuzzleHttp\Client($http_options);
        return $client;
    }

    /**
     * return a function for the retry handler which decides if a failed request should be retried
     *
     * @param int $max_retries maximum number of retries
     * @param \Psr\Log\LoggerInterface $logger logger
     * @return \Closure for the retry handler which decides if a failed request should be retried
     */
    protected static function createRetryDecider($max_retries, \Psr\Log\LoggerInterface $logger)
    {
        return function (
            $retries,
            \GuzzleHttp\Psr7\Request $request,
            \GuzzleHttp\Psr7\Response $response = null,
            \GuzzleHttp\Exception\TransferException $exception = null
        ) use (
            $max_retries,
            $logger
        ) {
            $retry = false;
            if ($response && $response->getStatusCode() >= 500) {
                $retry = true;
            }
            if ($exception instanceof \GuzzleHttp\Exception\ConnectException) {
                $retry = true;
            }
            if ($retry) {
                // stop if max_retries has been reached
                $retry = $retries < $max_retries;
                $log_action = $retry ? "retrying" : "stop retrying";
                $logger->warning(sprintf(
                    '%s %s/%s %s %s, %s',
                    $log_action,
                    $retries + 1,
                    $max_retries,
                    $request->getMethod(),
                    $request->getUri(),
                    $response ? 'status code: ' . $response->getStatusCode() : $exception->getMessage()
                ));
            }
            return $retry;
        };
    }

    /**
     * return a function for the retry handler which calculates the delay if a failed request is retried
     * uses an exponential function and the base_delay to calculate the delay (in milliseconds)
     *
     * @param int $base_delay base delay, delay for the first retry (in milliseconds)
     * @return  \Closure for the retry handler which calculates the delay if a failed request is retried
     */
    protected static function createRetryDelay($base_delay)
    {
        return function (int $retries) use ($base_delay) {
            return (int) $base_delay * pow(2, $retries - 1);
        };
    }

    /**
     *  getter for http_client
     *  if no http_client is set a http_client is created by createHttpClient()
     *  with the instance http_options and logger
     * @return \GuzzleHttp\Client
     */
    public function getHttpClient()
    {
        if (!isset($this->http_client)) {
            $this->http_client = self::createHttpClient($this->http_options, $this->logger);
        }
        return $this->http_client;
    }

    /**
     * setter for http_client
     * @param \GuzzleHttp\Client $http_client
     * @return $this
     */
    public function setHttpClient(\GuzzleHttp\Client $http_client)
    {
        $this->http_client = $http_client;
        return $this;
    }

    /**
     * reset http_client options and headers
     * @return $this
     */
    public function resetHttpClient()
    {
        $this->http_headers = [];
        $this->http_options = [];
        $this->http_response = null;
        $this->http_response_xml = null;
        return $this;
    }

    /**
     * getter for http client request headers
     * @return array with request headers
     */
    public function getHttpHeaders()
    {
        return $this->http_headers;
    }

    /**
     * getter for single http client request header
     * @param string name name of header
     * @return string value of header or null if not set
     */
    public function getHttpHeader(string $name)
    {
        return key_exists($name, $this->http_headers) ? $this->http_headers[$name] : null;
    }

    /**
     * setter for http client request headers
     * @param array $headers http request headers, see GuzzleHttp documentation
     * @return $this
     */
    public function setHttpHeaders(array $headers)
    {
        $this->http_headers = $headers;
        return $this;
    }

    /**
     * setter for single http client request header
     * @param string name name of header
     * @param string value value for header
     * @return $this
     */
    public function setHttpHeader(string $name, string $value)
    {
        $this->http_headers[$name] = $value;
        return $this;
    }

    /**
     * getter for http client options
     * @return array http client options, empty array if not set
     */
    public function getHttpOptions()
    {
        return $this->http_options;
    }

    /**
     * setter for http client options
     * @param array $options http client options, see GuzzleHttp documentation
     * @return $this
     */
    public function setHttpOptions(array $options)
    {
        $this->http_options = $options;
        return $this;
    }

    /**
     * get http response
     * @return \GuzzleHttp\Psr7\Response
     */
    public function getHttpResponse()
    {
        return $this->http_response;
    }

    /**
     * set http response
     * @param \GuzzleHttp\Psr7\Response $response
     * @return $this
     */
    public function setHttpResponse($response)
    {
        $this->http_response = $response;
        $this->http_response_xml = null;
        return $this;
    }

    /**
     * return the http status code of last http request
     * @return string http status code, empty string if no response is set
     */
    public function getHttpStatus()
    {
        if (isset($this->http_response)) {
            return $this->http_response->getStatusCode();
        } else {
            return "";
        }
    }

    /**
     *  return parsed response object of last http request
     * @return SimpleXMLElement
     */
    public function getHttpResponseXml()
    {
        // if already parsed or no response then done
        if (isset($this->http_response_xml) || !isset($this->http_response)) {
            return $this->http_response_xml;
        }

        $content_type = $this->http_response->getHeader("Content-Type") ?: ["NOT SET"];
        $content_type = $content_type[0]; // result from getHeader() is an array
        // parse reponse body if content type is xml
        // be generous, any string containing xm will do
        // e.g.: application/atom+xml;content='application/vnd.oclc.marc21+xml'
        if (preg_match("/xml/", $content_type)) {
            $body = (string) $this->http_response->getBody();
            $this->http_response_xml = $this->parseXml($body);
            if (!empty($this->http_response_xml)) {
                return $this->http_response_xml;
            }
            $errcode = "XML_PARSE_ERROR";
            $errmsg = "failed to parse xml response, errors are logged";
        } else {
            $errcode = "RESPONSE_NO_XML";
            $errmsg = "response does not contain xml, Content-Type: $content_type";
        }
        $this->ndcPush("getHttpResponseXml")->info($errmsg)->ndcPop();
        $this->http_response_xml = $this->handleParseXmlError($errcode, $errmsg);
        return $this->http_response_xml;
    }

    /**
     * send get request with the http_client
     * @param string $url
     * @param array $http_options optional http client options, is merged with options from getHttpOptions()
     * @return \GuzzleHttp\Psr7\Response
     */
    public function httpGet(string $url, array $http_options = [])
    {
        $this->debug("http get, url: $url");

        $options = $this->getMergedHttpOptions($http_options);
        return $this->httpRequest("GET", $url, $options);
    }

    /**
     * send post request with the http_client
     * @param type $url post url
     * @param type $body post data
     * @param array $http_options optional http client options, is merged with options from getHttpOptions()
     * @return \GuzzleHttp\Psr7\Response
     */
    public function httpPost(string $url, $body, array $http_options = [])
    {
        $this->debug("http post, url: $url, data size: " . strlen($body));

        $options = $this->getMergedHttpOptions($http_options);
        $options["body"] = $body;
        return $this->httpRequest("POST", $url, $options);
    }

    /**
     * send multipart post request with the http_client
     * this method can be used to upload files to a server
     * @param string $url post url
     * @param array $multipart post multipart data
     *        (see https://docs.guzzlephp.org/en/stable/request-options.html#multipart)
     * @param array $http_options optional http client options, is merged with options from getHttpOptions()
     * @return \GuzzleHttp\Psr7\Response
     */
    public function httpPostMultipart(string $url, $multipart, array $http_options = [])
    {
        $this->debug("http multipart post, url: $url, multipart count: " . count($multipart));

        $options = $this->getMergedHttpOptions($http_options);
        $options["multipart"] = $multipart;
        return $this->httpRequest("POST", $url, $options);
    }

    /**
     * send put request with the http_client
     * @param string $url put url
     * @param string $body put data
     * @param array $http_options optional http client options, is merged with options from getHttpOptions()
     * @return \GuzzleHttp\Psr7\Response
     */
    public function httpPut(string $url, $body, array $http_options = [])
    {
        $this->debug("http put, url: $url, data size: " . strlen($body));

        $options = $this->getMergedHttpOptions($http_options);
        $options["body"] = $body;
        return $this->httpRequest("PUT", $url, $options);
    }

    /**
     * low level routine to send http request with the http_client
     * @param string $method
     * @param string $uri
     * @param array $options contains headers, debug
     * @return \GuzzleHttp\Psr7\Response
     */
    public function httpRequest(string $method, string $url, array $options = [])
    {
        // log request timing stats
        $options['on_stats'] = function (\GuzzleHttp\TransferStats $stats) {
            $this->debug("transfer time: " . $stats->getTransferTime() . " sec.");
        };

        // catch all (curl) exceptions
        try {
            $this->setHttpResponse($this->getHttpClient()->request($method, $url, $options));
        } catch (\GuzzleHttp\Exception\TransferException $ex) {
            $this->setHttpResponse($this->handleHttpRequestException($ex, $method, $url, $options));
        }
        return $this->http_response;
    }

    /**
     * merge headers and options properties with passed options
     * @param array $options
     * @return array with merged headers and options
     */
    protected function getMergedHttpOptions(array $options)
    {
        $http_options = $this->getHttpOptions();
        // merge all the headers
        $http_options_headers = key_exists("headers", $http_options) ? $http_options["headers"] : [];
        $http_headers = $this->getHttpHeaders();
        $headers = key_exists("headers", $options) ? $options["headers"] : [];
        $headers = array_merge($http_options_headers, $http_headers, $headers);
        // merge all options
        $options = array_merge($http_options, $options);
        // apply merged headers
        if (!empty($headers)) {
            $options["headers"] = $headers;
        }
        return $options;
    }

    /**
     * handle http exception from http_client
     * can be overriden by class that uses this trait
     * @param \Exception $ex
     * @param string $method
     * @param string $url
     * @param array $options
     * @return \GuzzleHttp\Psr7\Response
     */
    protected function handleHttpRequestException(\Exception $ex, string $method, string $url, array $options)
    {
        $this->error("$method request exception: " . $ex->getMessage());
        $code = $ex->getCode();
        $msg = $ex->getMessage();
        $resp_body = "<exception><error code=$code><message>$msg</message></error></exception>";
        return new \GuzzleHttp\Psr7\Response(500, ["Content-Type" => "text/xml"], $resp_body);
    }

    /**
     * parse xml string
     * @param $text
     * @return SimpleXMLElement parsed xml or false on errors
     */
    protected function parseXml($text)
    {
        if (trim($text) == '') {
            $this->error("xml string is empty");
            return false;
        }

        libxml_use_internal_errors(true);
        $xml = simplexml_load_string($text);
        $errors = libxml_get_errors();
        libxml_clear_errors();
        $error_count = count($errors);
        if ($error_count > 0) {
            $this->error("parsing xml string failed with $error_count errors (max. 10 listed");
            // only log first 10 error messages
            foreach (array_slice($errors, 0, 9) as $error) {
                $this->error('xml parse error: ' . print_r($error, true));
            }
            return false;
        }
        return $xml;
    }

    /**
     * handle error message from getHttpResponseXml()
     * can be overriden by class that uses this trait
     * @param string $code the error code
     * @param string $message the error message
     * @return SimpleXMLElement
     */
    protected function handleParseXmlError(string $code, string $message)
    {
        $fakeResp = "<root><error status='500'><code>$code</code><message>$message</message></error></root>";
        return $this->parseXml($fakeResp);
    }
}
