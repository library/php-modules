<?php

namespace Library\Test;

/**
 * Convenience class for creating a \GuzzleHttp\Client
 * with a \GuzzleHttp\Handler\MockHandler
 * (see: https://docs.guzzlephp.org/en/stable/testing.html#mock-handler)
 * and request history
 * (see: https://docs.guzzlephp.org/en/stable/testing.html#history-middleware)
 *
 * Example usage:
 *
 * MockHttpClient::setDataDir(__DIR__ . "/data");
 *
 * $mock_client = \Library\Test\MockHttpClient([["file" => "testrecord1.xml"]];
 * $wq = new \Library\Webquery();
 * $wq->setHttpClient($mock_client);
 *
 * $response = $wq->httpGet("http://localhost/WebQuery/test/1");
 * $this->assertEquals(200, $response->getStatusCode(), "response status");
 *
 * $request = $mock_client->getHistory()[0]["request"];
 * $this->assertEquals("http://localhost/WebQuery/test/1", $request->getUri(), "request url");
 *
 */
class MockHttpClient extends \GuzzleHttp\Client
{
    /**
     * @var string directory path for files with mock response body
     */
    private static $data_dir = ".";

    /**
     *
     * @var array default headers for the mocked responses
     */
    private static $default_headers = ["Content-Type" => "text/xml"];

    /**
     * @var \GuzzleHttp\Handler\MockHandler the mock handler
     */
    private $mock_handler;

    /**
     * @var array with the request history
     */
    private $history;


    /**
     * Get path for directory with test data files
     *
     * @return string directory path to data files for response body
     */
    public static function getDataDir(): string
    {
        return self::$data_dir;
    }

    /**
     * Set path for directory with test data files
     * Convention: MockHttpClient::setDataDir(__DIR__ . "/data");
     *
     * @param string $dir path to data files for response body
     */
    public static function setDataDir(string $dir)
    {
        self::$data_dir = $dir;
    }


    /**
     * Construct \GuzzleHttp\Psr7\Response objects from array with
     * \GuzzleHttp\Psr7\Response objects
     * and/or
     * hashes with response 'parts'
     *
     * The hashes are user to construct \GuzzleHttp\Psr7\Response objects.
     * Allowed key/value pairs in the hashes:
     * status => (integer) http status code, default 200
     * headers => (array) array with response headers, default: $default_headers
     * body => (string) response body
     *   or
     * file => (string) name of file in $data_dir with response body
     *
     * @param array $responses
     * @return array with \GuzzleHttp\Psr7\Response objects
     */
    public static function constructResponses(array $responses): array
    {
        $result = [];
        foreach ($responses as $resp) {
            if (is_array($resp)) {
                # create \GuzzleHttp\Psr7\Response instance from hash
                $status = key_exists("status", $resp) ? $resp["status"] : 200;
                $headers = key_exists("headers", $resp) ? $resp["headers"] : self::$default_headers;
                $body = "";
                if (key_exists("body", $resp)) {
                    $body = $resp["body"];
                } elseif (key_exists("file", $resp)) {
                    $filename = self::$data_dir . "/" . $resp["file"];
                    if (file_exists($filename)) {
                        $body = new \GuzzleHttp\Psr7\Stream(fopen($filename, "r"));
                    } else {
                        $body = "FILE NOT FOUND: $filename";
                    }
                }
                $result[] = new \GuzzleHttp\Psr7\Response($status, $headers, $body);
            } else {
                # assume it is a \GuzzleHttp\Psr7\Response
                $result[] = $resp;
            }
        }
        return $result;
    }


    /**
     * Constructor
     *
     * @param array $responses array with response settings (see constructResponses())
     */
    public function __construct(array $responses = [])
    {
        $mock_responses = self::constructResponses($responses);
        $this->mock_handler = new \GuzzleHttp\Handler\MockHandler($mock_responses);
        $mock_stack = \GuzzleHttp\HandlerStack::create($this->mock_handler);

        $this->history = [];
        $mock_stack->push(\GuzzleHttp\Middleware::history($this->history));
        parent::__construct(["handler" => $mock_stack]);
    }

    /**
     * Add responses to the internal array with responses.
     *
     * @param array $responses array with response settings (see constructResponses())
     */
    public function addResponses(array $responses)
    {
        $mock_responses = self::constructResponses($responses);
        foreach ($mock_responses as $resp) {
            $this->mock_handler->append($resp);
        }
    }

    /**
     * Get count of responses that are set with constructor and addResponses()
     *
     * @return array with response settings
     */
    public function getResponsesCount(): int
    {
        return $this->mock_handler->count();
    }

    /**
     * Return the request history.
     *
     * @return array with \Psr7\Request objects
     */
    public function getHistory()
    {
        return $this->history;
    }
}
