<?php

namespace Library;

/**
 * Trait that adds support for getting/setting attributes/properties.
 * Useful for settings from a configuration file.
 * And no need to create getter/setter for each attribute.
 */
trait AttributesTrait
{
    /**
     * Attributes of instance
     *
     * @var array with key, value pairs
     */
    protected $attributes = [];

    /**
     * Add attributes to existing attributes
     *
     * @param array|\ArrayAccess $attributes
     * @return $this
     */
    public function addAttributes($attributes)
    {
        // array_merge does not work on \ArrayAccess objects (settings in Slim framework)
        foreach ($attributes as $key => $value) {
            $this->attributes[$key] = $value;
        }
        return $this;
    }

    /**
     * Set attributes, replaces all existing attributes
     *
     * @param array|\ArrayAccess $attributes new attributes
     * @return $this
     */
    public function setAttributes($attributes)
    {
        $this->attributes = [];
        return $this->addAttributes($attributes);
    }

    /**
     * Return array with all attributes
     *
     * @return array key/value array with all attributes
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * Set a key/value pair to the attributes
     *
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public function setAttribute(string $key, $value)
    {
        $this->attributes[$key] = $value;
        return $this;
    }

    /**
     * Return the value of the attribute with given key
     * if the attribute is not set then return the given default value
     *
     * @param string $key
     * @param mixed $default
     * @return mixed value of attribute with given key of the default value
     */
    public function getAttribute(string $key, $default = '')
    {
        return key_exists($key, $this->attributes) ? $this->attributes[$key] : $default;
    }
}
