<?php

namespace Library;

/**
 * Class for static utility methods
 */
class Util
{
    /**
     * Get value of first node that matches the xpath expression.
     * When multiple nodes match the xpath expression then
     * the value from the first matching node is returned.
     *
     * @param SimpleXMLElement $xml
     * @param string $path xpath expression
     * @param string $default default value if no node matches
     * @return string value of first matching node or default value (empty string)
     */
    public static function valueOfXpath(\SimpleXMLElement $xml, string $path, string $default = ""): string
    {
        $nodes = $xml->xpath($path);
        return empty($nodes) ? $default : (string) $nodes[0];
    }
}
