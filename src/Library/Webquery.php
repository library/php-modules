<?php

namespace Library;

class Webquery
{
    use \Library\AttributesTrait;
    use \Library\HttpClientTrait;

    /**
     * attributes
     *
     * valid keys:
     * - host
     * - service
     * - suffix
     * - record_name
     * - dryrun
     *
     * default values:
     * - host -> backend.library.wur.nl
     * - suffix -> xml
     */

    /**
     * @var string isn parameter
     */
    protected $isn;

    /**
     * @var string wq_ofs query parameter
     */
    protected $wq_ofs;

    /**
     * @var string wq_max query parameter
     */
    protected $wq_max;

    /**
     * @var query string
     */
    protected $query;

    /**
     * @var array with children and attributes from error element
     */
    protected $resp_error;

    /**
     * webquery error codes that are considered a success response
     * @var array with strings
     */
    protected $success_codes = [
        "WQW_NO_HITS",
        "WQW_RECORD_NOT_FOUND",
        "WQW_UPDATE_OK",
        "WQW_UPDATE_NOCHANGE"
    ];

    public function __construct()
    {
        $this->ndcPush('webquery');
        // add default attributes
        $this->addAttributes([
            "host" => "backend.library.wur.nl",
            "suffix" => "xml",
        ]);
    }


    /**
     * set host name for webquery url
     * @param string $host webquery host
     * @return $this
     */
    public function setHost($host)
    {
        return $this->setAttribute("host", $host);
    }


    /**
     * get host name for webquery url
     * @return string
     */
    public function getHost()
    {
        return $this->getAttribute("host");
    }


    /**
     * set webquery service name for url
     * @param string $service webquery service
     * @return $this
     */
    public function setService($service)
    {
        return $this->setAttribute("service", $service);
    }


    /**
     * get webquery service name
     * @return string
     */
    public function getService()
    {
        return $this->getAttribute("service");
    }


    /**
     * set webquery suffix for url
     * @param string $suffix webquery suffix
     * @return $this
     */
    public function setSuffix($suffix)
    {
        return $this->setAttribute("suffix", $suffix);
    }


    /**
     * get webquery suffix
     * @return string
     */
    public function getSuffix()
    {
        return $this->getAttribute("suffix");
    }


    /**
     * set name of record element
     * if not set then the service name is used as record name
     * @param string $record_name record element name
     * @return $this
     */
    public function setRecordName($record_name)
    {
        return $this->setAttribute("record_name", $record_name);
    }


    /**
     * get name of record element
     * if record_name is not set return service name
     * @return record name
     */
    public function getRecordName()
    {
        return $this->getAttribute("record_name", $this->getService());
    }


    /**
     * set value of dryrun attribute
     * this is used as response body in dryrun mode
     * @param string $dryrun
     * @return $this
     */
    public function setDryRun($dryrun)
    {
        return $this->setAttribute("dryrun", $dryrun);
    }


    /**
     * retrun true when in dryrun mode
     * @return boolean true if dryrun attribute is set (not empty).
     */
    public function isDryRun()
    {
        return !empty($this->getAttribute("dryrun"));
    }


    /**
     * return value of dryrun attribute
     * this is used as response body in dryrun mode
     * @return string value of dryrun attribute
     */
    public function getDryRun()
    {
        return $this->getAttribute("dryrun");
    }


    /**
     * set isn parameter for webquery search query
     * @param string $isn webquery isn parameter, not used in url if empty
     * @return $this
     */
    public function setIsn($isn)
    {
        $this->isn = $isn;
        return $this;
    }


    /**
     * set wq_ofs parameter for webquery search query
     * @param string $wq_ofs webquery wq_ofs parameter
     * @return $this
     */
    public function setWqOfs($wq_ofs)
    {
        $this->wq_ofs = $wq_ofs;
        return $this;
    }

    /**
     * set wq_max parameter for webquery search query
     * @param string $wq_max webquery wq_max parameter
     * @return $this
     */
    public function setWqMax($wq_max)
    {
        $this->wq_max = $wq_max;
        return $this;
    }


    /**
     * set query parameters
     * this can be either a string or an array with key/value pairs
     * the keys are the parameter keys, if a key is prefixed with an exclamation mark
     * the it means 'AND NOT'
     * the values can be either a single value or an array with values
     * when an array with values is given it will be interpreted as a list
     * of values with all the same key. e.g 'key' => ['v1, v2] becomes
     * 'key=v1&key=v2'
     * @param string|array $query query string or key/value array
     * @return $this
     */
    public function setQuery($query)
    {
        $this->query = $query;
        return $this;
    }


    /**
     * create query string from query parameters
     *
     * @param string $query string or array with key/value pairs
     * @param string $wq_ofs value for wq_ofs
     * @param string $wq_max value for wq_max
     * @return string
     */
    public static function getQueryString($query, $wq_ofs = null, $wq_max = null)
    {
        if (empty($query)) {
            return "";
        }

        if (is_string($query)) {
            $result = $query;
            if (isset($wq_max)) {
                $result = "wq_max=$wq_max&$result";
            }
            if (isset($wq_ofs)) {
                $result = "wq_ofs=$wq_ofs&$result";
            }
            return $result;
        }

        if (is_array($query)) {
            $result = [];
            if (isset($wq_ofs)) {
                $result[] = "wq_ofs=$wq_ofs";
            }
            if (isset($wq_max)) {
                $result[] = "wq_max=$wq_max";
            }
            foreach ($query as $key => $value) {
                # check for 'and not' exclamation mark before key
                $is_wqnot = strpos($key, "!") === 0;
                if ($is_wqnot) {
                    # remove exclamation mark from key
                    $key = substr($key, 1);
                    $result[] = "wq_rel=AND+NOT";
                }
                # tread a single value as an array with one value
                # one value is special case of key=val1&key=val2
                $values = is_array($value) ? $value : [$value];

                # surround multiple OR values with parentheses if rel = and not
                $use_open_close = $is_wqnot && count($values) > 1;
                if ($use_open_close) {
                    $result[] = "wq_par=open";
                }
                foreach ($values as $v) {
                    $result[] = "$key=" . rawurlencode($v);
                }
                if ($use_open_close) {
                    $result[] = "wq_par=close";
                }
            }
            return implode("&", $result);
        }

        return "";
    }


    /**
     * create url for search query from properties
     * uses host, service, suffix, wq_ofs, wq_max and query (if set)
     * @return string url
     */
    public function getSearchUrl()
    {
        $url = "http://" . $this->getHost() . "/WebQuery/" . $this->getService() . "/" . $this->getSuffix();
        if (!empty($this->isn)) {
            $url .= "/$this->isn";
        }
        $qry = $this->getQueryString($this->query, $this->wq_ofs, $this->wq_max);
        if (!empty($qry)) {
            $url .= "?$qry";
        }
        return $url;
    }


    /**
     * create url for createRecord method from properties
     * uses host, service and suffix (if set)
     * @return string url
     */
    public function getNewUrl()
    {
        $host = $this->getHost();
        $service = $this->getService();
        $suffix = $this->getSuffix();
        return "http://$host/WebQuery/$service/new_$suffix";
    }

    /**
     * create url for updateRecord method from properties
     * uses host, service, suffix (if set) and isn
     * @return string url
     */
    public function getUpdateUrl()
    {
        $host = $this->getHost();
        $service = $this->getService();
        $suffix = $this->getSuffix();
        return "http://$host/WebQuery/$service/update_$suffix/$this->isn";
    }

    /**
     * run a search request with set request parameters
     * results can be obtained with isSuccess(), getHits(), get record(), getRecords(),
     * hasNext(), getNextOfs() or just getHttpResponse() and getHttpResponseXml()
     * @return $this
     */
    public function search()
    {
        $this->ndcPush('search');
        $this->resetResponse();

        if (empty($this->getService())) {
            return $this->error("search: service is not set")->ndcPop();
        }

        $this->httpGet($this->getSearchUrl());

        return $this->ndcPop();
    }


    /**
     * run a search request with set request parameters
     * continue searches and collect all records until no next element is found
     * @return array all collected records from the search or false if any search fails
     */
    public function searchAllRecords()
    {
        $this->ndcPush('searchAllRecords');

        if (! $this->search()->isSuccess()) {
            $this->warning("search failed, stopping")->ndcPop();
            return false;
        }

        $records = $this->getRecords();
        while ($ofs = $this->getNextOfs()) {
            if (! $this->setWqOfs($ofs)->search()->isSuccess()) {
                $this->warning("search failed, stopping")->ndcPop();
                return false;
            }
            $records = array_merge($records, $this->getRecords());
        }

        $record_name = $this->getRecordName();
        $record_count = empty($records) ? 0 : count($records);
        $this->debug("number of $record_name records found: $record_count");
        $this->ndcPop();
        return $records;
    }


    /**
     * @return first record from search response or false
     */
    public function getRecord()
    {
        $xml = $this->getHttpResponseXml();
        if (!isset($xml)) {
            return false;
        }

        $records = $xml->xpath("//" . $this->getRecordName());
        if (empty($records)) {
            return false;
        }

        return $records[0];
    }


    /**
     * @return all records from search response or false
     */
    public function getRecords()
    {
        $xml = $this->getHttpResponseXml();
        if (!isset($xml)) {
            return false;
        }

        return $xml->xpath("//" . $this->getRecordName());
    }

    /**
     * return value of the hits element in search response
     * @return int number of hits or false if there is no valid search response
     */
    public function getHits()
    {
        $xml = $this->getHttpResponseXml();
        if (!isset($xml)) {
            return false;
        }

        $hits = $xml->xpath("//hits");
        return empty($hits) ? false : (string) $hits[0];
    }


    /**
     * @return boolean true if search response contains next element
     */
    public function hasNext()
    {
        $xml = $this->getHttpResponseXml();
        if (!isset($xml)) {
            return false;
        }

        $next = $xml->xpath("//next");
        return !empty($next);
    }


    /**
     * return value of wq_ofs attribute of next element in search response
     * @return false|string value of next wq_ofs or false if there is no valid search response
     */
    public function getNextOfs()
    {
        $xml = $this->getHttpResponseXml();
        if (!isset($xml)) {
            return false;
        }

        $next = $xml->xpath("//next");
        if (empty($next)) {
            return false;
        }
        $attr = $next[0]->attributes();
        return empty($attr['wq_ofs']) ? false : (string) $attr['wq_ofs'];
    }


    /**
     * convert xml tree to x-www-form-urlencoded string
     * @param \SimpleXMLElement $xml  xml tree
     * @return string xml as x-www-form-urlencoded string
     */
    public function xmlToWwwform($xml)
    {
        $result = [];
        foreach ($xml as $key => $value) {
            $result[] = "$key=" . ($value->count() > 0 ? '&' . $this->xmlToWwwform($value) : urlencode($value));
        }
        return implode('&', $result);
    }

    /**
     * convert x-www-form-urlencoded string to form-data array
     * @param string $instr
     * @return array form-data array
     */
    public function wwwFormToFormdata(string $instr): array
    {
        $result = [];
        $data_array = explode('&', $instr);

        foreach ($data_array as $data_obj) {
            $data_item = explode('=', $data_obj);
            $result[] = [
                'name' => $data_item[0],
                'contents' => $data_item[1]
            ];
        }

        return $result;
    }

    /**
     * create new record in webquery
     * uses properties host, service and suffix for the url
     * @param \SimpleXMLElement $xml post data (xml tree)
     * @param string $file full path to file to send to WQ
     * @param string $file_field_name field target name in WQ config
     * @return boolean true on success
     */
    public function createRecord($xml, $file = '', $file_field_name = '')
    {
        $this->ndcPush('createRecord');
        $this->resetResponse();

        if (empty($this->getService())) {
            $this->error("createRecord: service is not set")->ndcPop();
            return false;
        }

        $metadata = $this->xmlToWwwform($xml);

        // if we have a file, create multipart and use multipart POST
        if ($file !== '') {
            $metadata_part = $this->wwwFormToFormdata($metadata);
            $file_part = [
                'name'     => $file_field_name,
                'contents' => \GuzzleHttp\Psr7\Utils::tryFopen($file, 'r')
            ];

            // create multipart
            $multipart = $metadata_part;
            $multipart[] = $file_part;

            $this->httpPostMultipart($this->getNewUrl(), $multipart);
        } else {
            $this->httpPost($this->getNewUrl(), $metadata);
        }
        $success = $this->isSuccess();
        $code = $this->getErrorCode();
        $message = $this->getErrorMessage();
        if ($success) {
            $this->info("success, code=$code, message=$message, new isn=" . $this->getNewIsn());
        } else {
            $this->error("failed, code=$code, message=$message");
        }
        $this->ndcPop();
        return $success;
    }


    /**
     * update existing record in webquery
     * uses properties host, service and suffix for the url
     *
     * @param string|\SimpleXMLElement $form_data post data (x-www-form-urlencoded string)
     * @param boolean $skipfirstelement instructs to leave out the first element
     *                (usa. the record name) as this should not be part of the update string.
     *                If the data is retrieved from a WQ request it may contain the recordname
     *                and recordname-set. Both should not be present in the update string.
     *                This parameter has no effect if the parameter $form_data is already
     *                a string instead of a simplexml oject
     * @param string $file full path to file to send to WQ
     * @param string $file_field_name field target name in WQ config
     * @return boolean true on success
     */
    public function updateRecord(
        $form_data,
        $skipfirstelement = false,
        $file = '',
        $file_field_name = ''
    ) {
        $this->ndcPush('updateRecord');
        $this->resetResponse();
        // if the data is a SimpleXml object, serialize it
        if (!is_string($form_data)) {
            //$form_data = urlencode($this->stringifyXml($form_data));
            $form_data = $this->stringifyXml($form_data, $skipfirstelement);
        }

        if (empty($this->getService())) {
            $this->error("updateRecord: service is not set")->ndcPop();
            return false;
        }

        // if we have a file, create multipart and use multipart POST
        if ($file !== '') {
            $metadata_part = $this->wwwFormToFormdata($form_data);
            $file_part = [
                'name'     => $file_field_name,
                'contents' => \GuzzleHttp\Psr7\Utils::tryFopen($file, 'r')
            ];

            // create multipart
            $multipart = $metadata_part;
            $multipart[] = $file_part;

            $this->httpPostMultipart($this->getUpdateUrl(), $multipart);
        } else {
            $this->httpPost($this->getUpdateUrl(), $form_data);
        }

        $success = $this->isSuccess();
        $code = $this->getErrorCode();
        $message = $this->getErrorMessage();
        if ($success) {
            $this->info("success, code=$code, message=$message");
        } else {
            $this->error("failed, code=$code, message=$message");
        }
        $this->ndcPop();
        return $success;
    }


    /**
     * return success status of last webquery action
     * @return boolean true on success
     */
    public function isSuccess()
    {
        if ($this->getHttpStatus() != 200) {
            return false;
        }

        // check status attribute of webquery error element if available
        $error_status = $this->getErrorStatus();
        if (!empty($error_status) && $error_status != 200) {
            return false;
        }

        // check code attribute of webquery error element if available
        $error_code = $this->getErrorCode();
        if (!empty($error_code)) {
            // check if code is known to be a success indication
            return in_array($error_code, $this->success_codes);
        }

         return true;
    }


    /**
     * return content of first webquery error element of last webquery action
     * @return hash array with all webquery error attributes and elements
     */
    public function getError()
    {
        if (isset($this->http_response) && empty($this->resp_error)) {
            $xml = $this->getHttpResponseXml();
            $error = $xml->error[0];
            if (isset($error)) {
                foreach ($error->attributes() as $attr => $value) {
                    $this->resp_error[$attr] = (string) $value;
                }
                foreach ($error->children() as $key => $value) {
                    $this->resp_error[$key] = (string) $value;
                }
            }
        }
        return $this->resp_error;
    }


    /**
     * return value of given error property of last webquery action
     * @param string $name webquery error property name (attribute or element)
     * @return string value or empty string if not set
     */
    public function getErrorValue($name)
    {
        $error = $this->getError();
        return isset($error[$name]) ? $error[$name] : '';
    }


    /**
     * return value of webquery error code of last webquery action
     * @return string value of code error property or empty string
     */
    public function getErrorCode()
    {
        return $this->getErrorValue('code');
    }


    /**
     * return value of webquery error status of last webquery action
     * @return string value of status error property or empty string
     */
    public function getErrorStatus()
    {
        return $this->getErrorValue('status');
    }


    /**
     * return value of webquery error message of last webquery action
     * the return message is a composite of the message, field and xmlerror properties
     * @return string value of message error property or empty string
     */
    public function getErrorMessage()
    {
        $message = $this->getErrorValue('message');
        $field = $this->getErrorValue('field');
        if (!empty($field)) {
            $message = "$message: $field";
        }
        $xmlerror = $this->getErrorValue('xmlerror');
        return empty($xmlerror) ? $message : "$message ($xmlerror)";
    }


    /**
     * return value of webquery error isn of last webquery action
     * isn is set even if webquery failed to create a record
     * @return string value of isn error property or empty string
     */
    public function getNewIsn()
    {
        return $this->getErrorValue('isn');
    }


    /**
     * reset response properties
     * @return $this
     */
    protected function resetResponse()
    {
        $this->resp_error = [];
        return $this;
    }


    /**
     * send get request with the http_client
     * @param string $url
     * @param array $http_options optional http client options, is merged with options from getHttpOptions()
     * @return \GuzzleHttp\Psr7\Response
     */
    public function httpGet(string $url, array $http_options = [])
    {
        $this->debug("http get, url: $url");

        if ($this->isDryRun()) {
            $this->debug("dryrun: returning dryrun response");
            $this->setHttpResponse(new \GuzzleHttp\Psr7\Response(200, [], $this->getDryRun()));
            return $this->getHttpResponse();
        }

        $options = $this->getMergedHttpOptions($http_options);
        // ensure timeout is set
        if (!key_exists("timeout", $options)) {
            $options["timeout"] = 10;
        }
        // disable exceptions on 4xx and 5xx responses
        $options["http_errors"] = false;

        return $this->httpRequest("GET", $url, $options);
    }


    /**
     * run post request with the http_client
     * @param string $url post url
     * @param string $body post data
     * @param array $http_options optional http client options, is merged with options from getHttpOptions()
     * @return \GuzzleHttp\Psr7\Response
     */
    public function httpPost(string $url, $body, array $http_options = [])
    {
        $this->debug("http post, url: $url, data size: " . strlen($body));

        if ($this->isDryRun()) {
            $this->debug("dryrun: returning dryrun response");
            $this->setHttpResponse(new \GuzzleHttp\Psr7\Response(200, [], $this->getDryRun()));
            return $this->getHttpResponse();
        }

        $options = $this->getMergedHttpOptions($http_options);
        // force content-type header
        $headers = key_exists("headers", $options) ? $options["headers"] : [];
        $headers["Content-Type"] = ["application/x-www-form-urlencoded; charset=UTF-8"];
        $options["headers"] = $headers;
        // ensure timeout is set
        if (!key_exists("timeout", $options)) {
            $options["timeout"] = 10;
        }
        // disable exceptions on 4xx and 5xx responses
        $options["http_errors"] = false;
        // set body
        $options["body"] = $body;

        return $this->httpRequest("POST", $url, $options);
    }

    /**
     * send multipart post request with the http_client
     * this method can be used to upload files to a server
     * @param string $url post url
     * @param array $multipart post multipart data
     *        (see https://docs.guzzlephp.org/en/stable/request-options.html#multipart)
     * @param array $http_options optional http client options, is merged with options from getHttpOptions()
     * @return \GuzzleHttp\Psr7\Response
     */
    public function httpPostMultipart(string $url, $multipart, array $http_options = [])
    {
        $this->debug("http multipart post, url: $url, multipart count: " . count($multipart));

        if ($this->isDryRun()) {
            $this->debug("dryrun: returning dryrun response");
            $this->setHttpResponse(new \GuzzleHttp\Psr7\Response(200, [], $this->getDryRun()));
            return $this->getHttpResponse();
        }

        $options = $this->getMergedHttpOptions($http_options);
        // ensure timeout is set
        if (!key_exists("timeout", $options)) {
            $options["timeout"] = 10;
        }
        // disable exceptions on 4xx and 5xx responses
        $options["http_errors"] = false;
        // set multipart array/data
        $options["multipart"] = $multipart;

        return $this->httpRequest("POST", $url, $options);
    }

    /**
     * handle http exception from http_client
     * can be overriden by class that uses this trait
     * @param \Exception $ex
     * @param string $method
     * @param string $url
     * @param array $options
     * @return \GuzzleHttp\Psr7\Response
     */
    protected function handleHttpRequestException(\Exception $ex, string $method, string $url, array $options)
    {
        $code = $ex->getCode();
        $msg = $ex->getMessage();
        $this->error("$method request exception: $msg");
        $resp_body = "<exception><error code=$code><message>$msg</message></error></exception>";
        return new \GuzzleHttp\Psr7\Response(500, ["Content-Type" => "text/xml"], $resp_body);
    }


    /**
     * Wrapper around strinify to be able to remove the last &
     * @param \SimpleXMLElement $inputXml
     * @param bool $skipFirstElement
     * @return string
     */
    public function stringifyXml(\SimpleXMLElement $inputXml, bool $skipFirstElement)
    {
        $result = $this->stringify($inputXml);
        // if the first part is the entityname, remove it
        $asArray = explode("&", $result);
        if ($skipFirstElement) {
            array_shift($asArray);
        }
        // add crc if it's available
        $crc = $this->getAttribute("crc");
        if (!empty($crc)) {
            array_unshift($asArray, "wq_crc=$crc");
        }
        // skip last &
        if (empty($asArray[count($asArray)])) {
            array_pop($asArray);
        }
        return $result = implode("&", $asArray);
    }

    /**
     * Creates a string of alle elements of a SimleXML object
     * @param \SimpleXMLElement $inputXml
     * @return string
     */
    private function stringify(\SimpleXMLElement $inputXml)
    {
        $retstring = "";
        foreach ($inputXml->children() as $child) {
            $retstring .= $child->getName() . "=";
            // get text content, and remove newlines
            $retstring .= preg_replace('/\n\s*/', "", strip_tags($child->__toString())) . "&";
            if ($child->children()->count()) {
                // recursie
                $retstring .= $this->stringify($child);
            }
        }
        return $retstring;
    }
}
