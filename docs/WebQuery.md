# Webquery Examples

Set all attributes at once (from a configuration file e.g.)
~~~
use Library\Webquery;

class Example
{
    private $wq;

    public function __construct()
    {
        $attribs = [
            "host" => "myhost",
            "service" => "myservice",
            "suffix" => "xml",
            "record_name" => "a_world_record",
        ];
        $this->wq = new Webquery()->setAttributes($attribs);
    }
}
~~~

Set attributes with methods.
~~~
class Example
{
    private $wq;

    public function __construct()
    {
        $this->wq = new Webquery()
            ->setHost("myhost")
            ->setService("myservice")
            ->setSuffix("xml")
            ->getRecordName("a_world_record");
    }

}
~~~

Get a record.
~~~
class Example
{

    public function getEdepotRecord(string $isn)
    {
        $wq = new Webquery()
             ->setService("edepot")
             ->setRecordName("edepot")
             ->setIsn($edepot_isn)
             ->search();
        if (!$wq->isSuccess()) {
            $code = $wq->getErrorCode();
            $msg = $wq->getErrorMessage();
            $this->error("failed to get edepot record: $edepot_isn, wq error: $code $msg");
            return false;
        }
        $this->debug("found edepot record: $edepot_isn");
        return $wq->getRecord();
    }
}
~~~

Get multiple records with one method call.<br>
Do not use this example in the real world. :-)
~~~
class Example
{

    public function getAllEdepotRecords()
    {
        $wq = new Webquery()
            ->setService("edepot")
            ->setRecordName("edepot")
            ->setQuery('record=*')
            ->searchAllRecords();
        if (!$wq->isSuccess()) {
            $code = $wq->getErrorCode();
            $msg = $wq->getErrorMessage();
            $this->error("failed to get edepot records, wq error: $code $msg");
            return false;
        }
        return $wq->getRecord();
    }
}
~~~

Get multiple records by using next/ofs.
~~~
class Example
{

    public function getAllEdepotRecords()
    {
        $wq = new Webquery()
            ->setService("edepot")
            ->setRecordName("edepot")
            ->setQuery('record=*')
            ->search();

        if (! $wq->isSuccess()) {
            $this->warning("search failed, stopping")->ndcPop();
            return false;
        }

        $records = $this->getRecords();
        while ($ofs = $this->getNextOfs()) {
            if (! $this->setWqOfs($ofs)->search()->isSuccess()) {
                $this->warning("search failed, stopping");
                return false;
            }
            $records = array_merge($records, $this->getRecords());
        }

        return $records;
    }
}
~~~

Add a record.
~~~
class Example
{

    public function addRecord($record)
    {
        $wq = new Webquery()
            ->setHost("myhost")
            ->setService("myservice")
            ->setSuffix("xml")
            ->createRecord($record);
        if (!$wq->isSuccess()) {
            $code = $wq->getErrorCode();
            $msg = $wq->getErrorMessage();
            $this->error("failed to add record, wq error: $code $msg");
            return false;
        }
        return true;
    }
}
~~~

Update a record.
~~~
class Example
{

    public function addRecord()
    {
        $wq = new Webquery()
            ->setHost("myhost")
            ->setService("myservice")
            ->setIsn(123)
            ->updateRecord("a=&a=1&b=2");
        if (!$wq->isSuccess()) {
            $code = $wq->getErrorCode();
            $msg = $wq->getErrorMessage();
            $this->error("failed to update record, wq error: $code $msg");
            return false;
        }
        return true;
    }
}
~~~
