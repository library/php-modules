# LoggerTrait Examples

## Initialisation example with settings hash with handlers
This is the preferred method for initialisation.<br>
The settings hash will come in most cases from a configuration file.
~~~
class Example
{
    use \Library\LoggerTrait;


    public function __construct()
    {
        $settings = [
            "name" => "test-logger",
            "handler" => [
                [
                    "path" => "log/test.log",
                    "level" => "debug",
                    "format" => "[%datetime%]%level_name% %extra% %context%: %message%", // not testable
                ],
                [
                    "path" => "php://stdout",
                    "level" => "warning",
                    "format" => "[%datetime%]%level_name% %extra% %context%: %message%", // not testable
                ],
            ],
        ];
        $this->initLogger($settings);
    }
}
~~~

## Initialisation example with simple settings hash with one implicit handler
~~~
class Example
{
    use \Library\LoggerTrait;


    public function __construct()
    {
        $settings = [
            "name" => "test-logger",
            "path" => "log/test.log",
            "level" => "debug",
            "format" => "[%datetime%]%level_name% %extra% %context%: %message%",
        ];
        $this->initLogger($settings);
    }
}
~~~

##Nested Diagnostic Context (NDC) example
When using NDC every log message enriched with the content of the NDC<br>
so you don't have to add that information to every log message.<br>
The NDC works like a stack so you push and pop context information.
~~~
class Example
{
    use \Library\LoggerTrait;


    public function DoSomething($isn)
    {
        $this->ndcPush("isn=$isn");
        .....
        if ($error) {
            # "isn=$isn" is automatically added to error log message
            $this->error($error);
            $this->ndcPop();
            return;
        }
        .....
        $this->ndcPop();
        return $result;
    }
}
~~~
