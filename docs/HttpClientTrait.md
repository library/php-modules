# HttpClientTrait Examples

A http client can be created with the static class method
~~~
class Example
{
    use \Library\HttpClientTrait;

    public function initClient()
    {
        # for allowed options, see https://docs.guzzlephp.org/en/stable/request-options.html
        $options = ["max_retries" => 3, "base_delay" => 100];
        $this->http_client = \Library\HttpClientTrait::createHttpClient($options, $this->logger);
    }

    public function doSomthing()
    {
        $http_client = $this->getHttpClient();
        .....
    }
}
~~~

Or you can lazily create an equivalent http client.
~~~
class Example
{
    use \Library\HttpClientTrait;

    public function __construct()
    {
        $this->setHttpOptions(["max_retries" => 3, "base_delay" => 100]);
    }

    public function doSomething()
    {
        $http_client = $this->getHttpClient();
        .....
    }
}
~~~

Make a http GET request for xml data
~~~
class Example
{
    use \Library\HttpClientTrait;

    public function makeGetRequest()
    {
        $http_client = $this->getHttpClient();

        # do a GET http request
        $response = $http_client->httpGet("http://localhost/1");
        # test http status
        if ($response->getStatusCode() != "200") {
            return; # failure!
        }

        # or equivalent
        $http_client->httpGet("http://localhost/1");
        # test http status
        if ($http_client->getHttpStatus() != "200") {
            return; # failure!
        }

        # get XML data from response
        $xml = $http_client->getHttpResponseXml();
    }
}
~~~
